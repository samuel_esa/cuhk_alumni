<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Fusonic\OpenGraph\Consumer;
use Illuminate\Support\Facades\Hash;
Route::get('/', function () {

  //$user = new App\User();
  //$user->password = Hash::make('123123123');
  //$user->email = 'the-email@example.com';
  //$user->name = 'My Name';
  //$user->save();

  $lang = session('lang');

  if ($lang == 'zh-hk' || $lang = null || $lang == '') {
    return redirect('/zh-hk');
  }else if($lang == 'en'){
    return redirect('/en');
  }

});

Route::group(['prefix' => 'admin'], function() {
  Route::auth();
});

#News****************************************************************************************


Route::get('/webform', function () {

  $locale = App::getLocale();

  $data = [];
  $data['lang'] = $locale;
  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();
  $data['buttons'] = DB::table('buttons')->get()->toArray();
  return view('news_new', $data);
});

Route::post('/action/upload/images', 'UploadController@storeImages');
Route::post('/action/insert/news', 'InsertController@insertNews');

Route::get('/news/success', function () {

  $locale = App::getLocale();

  $data = [];
  $data['lang'] = $locale;
  $data['buttons'] = DB::table('buttons')->get()->toArray();
  return view('news_success', $data);
});

Route::post('/news/recaptcha', 'RecaptchaController@check');

#General****************************************************************************************

Route::get('/{locale}', function ($locale) {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $issue = DB::table('issue')->where('publish', 1)->orderBy('date', 'desc')->first();
  $issue_id = $issue->id;

  $data = [];
  $data['lang'] = $locale;

  $data['cover_news'] = DB::table('passage')
    ->where('passage_publish', '1')
    ->where('issue_id', $issue_id)
    ->where('is_cover', 1)
    ->first();

  $data['issues'] = DB::table('issue')->where('id', '!=', $issue_id)->where('publish', 1)->orderBy('date', 'desc')->get()->toArray();

  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();

  foreach ($data['categories'] as $category) {
    if($category->type == 3 && $issue->alumni_link != ''){

      $process_link = explode("/", $issue->alumni_link);
      $link_lang = '';

      if($process_link[2] == "www.cuhk.edu.hk" && ($process_link[3] == "chinese" || $process_link[3] == "english")){
        if ($locale == 'zh-hk') {
          $process_link[3] = "chinese";
        }else if($locale == 'en'){
          $process_link[3] = "english";
        }
      }

      $after_link = implode('/', $process_link);

      //$category->alumni = OpenGraph::fetch($after_link);
      $consumer = new Consumer();
      $category->alumni = $consumer->loadUrl($after_link);
    }

    $category->passages = DB::table('passage')
    ->join('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->select('passage.*', 'issue.date', 'category.name_zh')
    ->where('passage.passage_publish', '1')
    ->where('issue.id', $issue_id)
    ->where('passage.category_id', $category->id)
    ->orderBy(($category->type == 4 ? 'passage.event_date' : 'passage.sequence'), 'ASC')
    ->orderBy('passage.create_date', 'ASC')
    ->get();
  }

  $data['issue'] = $issue;
  $data['path'] = '';

  $data['banner'] = DB::table('banner')
  ->where('publish', '1')
  ->where('start_date', '<=', date("Y-m-d"))
  ->where('end_date', '>=', date("Y-m-d"))
  ->first();

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('index', $data);
});

#Issue****************************************************************************************
Route::get('/{locale}/issue/{issue_date?}', function ($locale, $issue_date = '') {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $issue = null;

  if($issue_date == ''){
    return redirect('/');
  }else{
    $issue = DB::table('issue')->where('date', $issue_date)->where('publish', 1)->orderBy('id', 'desc')->first();
  }

  $data = [];
  $data['lang'] = $locale;

  if($issue == null){
    $data['date'] = '找不到對應期刊';
    return view('index', $data);
  }

  $data['cover_news'] = DB::table('passage')
    ->where('passage_publish', '1')
    ->where('issue_id', $issue->id)
    ->where('is_cover', 1)
    ->first();

  $data['issues'] = DB::table('issue')->where('id', '!=', $issue->id)->where('publish', 1)->orderBy('date', 'desc')->get()->toArray();

  if($issue == null){
    return view('index');
  }

  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();

  foreach ($data['categories'] as $category) {
    if($category->type == 3 && $issue->alumni_link != ''){

      $process_link = explode("/", $issue->alumni_link);
      $link_lang = '';

      if($process_link[2] == "www.cuhk.edu.hk" && ($process_link[3] == "chinese" || $process_link[3] == "english")){
        if ($locale == 'zh-hk') {
          $process_link[3] = "chinese";
        }else if($locale == 'en'){
          $process_link[3] = "english";
        }
      }

      $after_link = implode('/', $process_link);

      //$category->alumni = OpenGraph::fetch($after_link);
      $consumer = new Consumer();
      $category->alumni = $consumer->loadUrl($after_link);
    }

    $category->passages = DB::table('passage')
    ->join('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->select('passage.*', 'issue.date', 'category.name_zh')
    ->where('passage.passage_publish', '1')
    ->where('issue.id', $issue->id)
    ->where('passage.category_id', $category->id)
    ->orderBy(($category->type == 4 ? 'passage.event_date' : 'passage.sequence'), 'ASC')
    ->orderBy('passage.create_date', 'ASC')
    ->get();
  }

  $data['issue'] = $issue;
  $date = new DateTime($issue->date);
  $data['date'] = $date->format('Y年n月號');
  $data['path'] = "/issue/" . $issue_date;

  $data['banner'] = DB::table('banner')
  ->where('publish', '1')
  ->where('start_date', '<=', date("Y-m-d"))
  ->where('end_date', '>=', date("Y-m-d"))
  ->first();

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('index', $data);
});

Route::get('/{locale}/search', function ($locale) {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $data = array();
  $data['lang'] = $locale;

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('search', $data);
});

Route::post('/action/search/{locale}', 'SearchController@searchPassage');

#Passage****************************************************************************************
Route::get('/{locale}/issue/{issue_date}/{issue_id?}', function ($locale, $issue_date, $issue_id = '') {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $issue = DB::table('issue')->where('date', $issue_date)->orderBy('id', 'desc')->first();

  if($issue == null){
    return view('index');
  }

  $data = [];
  $data['lang'] = $locale;

  $data['issues'] = DB::table('issue')->where('id', '!=', $issue->id)->orderBy('date', 'desc')->get()->toArray();

  $data['passage'] = DB::table('passage')->where('id', $issue_id)->first();

  $data['issue'] = $issue;

  $data['path'] = '/issue/' . $issue_date . '/' . $issue_id;

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('passage', $data);
});



#Backend****************************************************************************************
Route::get('/admin/issues', function () {
    return view('admin.issue');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/issues/new', function () {
  return view('admin.issue_new');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/issues/{id}', function ($id) {
  $data = [];
  $data['id'] = $id;
  $data['issue'] = DB::table('issue')->where('id', $id)->first();
  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();
  return view('admin.issue_edit', $data);
})->middleware('auth')->middleware('inGroup:2');

#Backend Preview Issue ****************************************************************************************
Route::get('/{locale}/preview/issue/{issue_date}', function ($locale, $issue_date) {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $issue = null;

  if($issue_date == ''){
    return redirect('/');
  }else{
    $issue = DB::table('issue')->where('date', $issue_date)->orderBy('id', 'desc')->first();
  }

  $data = [];
  $data['lang'] = $locale;

  if($issue == null){
    $data['date'] = '找不到對應期刊';
    return view('index', $data);
  }

  $data['cover_news'] = DB::table('passage')
    ->where('passage_publish', '1')
    ->where('issue_id', $issue->id)
    ->where('is_cover', 1)
    ->first();

  $data['issues'] = DB::table('issue')->where('id', '!=', $issue->id)->where('publish', 1)->orderBy('date', 'desc')->get()->toArray();

  if($issue == null){
    return view('index');
  }

  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();

  foreach ($data['categories'] as $category) {
    if($category->type == 3 && $issue->alumni_link != ''){

      $process_link = explode("/", $issue->alumni_link);
      $link_lang = '';

      if($process_link[2] == "www.cuhk.edu.hk" && ($process_link[3] == "chinese" || $process_link[3] == "english")){
        if ($locale == 'zh-hk') {
          $process_link[3] = "chinese";
        }else if($locale == 'en'){
          $process_link[3] = "english";
        }
      }

      $after_link = implode('/', $process_link);

      //$category->alumni = OpenGraph::fetch($after_link);
      $consumer = new Consumer();
      $category->alumni = $consumer->loadUrl($after_link);
    }

    $category->passages = DB::table('passage')
    ->join('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->select('passage.*', 'issue.date', 'category.name_zh')
    ->where('passage.passage_publish', '1')
    ->where('issue.id', $issue->id)
    ->where('passage.category_id', $category->id)
    ->orderBy(($category->type == 4 ? 'passage.event_date' : 'passage.sequence'), 'ASC')
    ->orderBy('passage.create_date', 'ASC')
    ->get();
  }

  $data['issue'] = $issue;
  $date = new DateTime($issue->date);
  $data['date'] = $date->format('Y年n月號');

  $data['banner'] = DB::table('banner')
  ->where('publish', '1')
  ->where('start_date', '<=', date("Y-m-d"))
  ->where('end_date', '>=', date("Y-m-d"))
  ->first();

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('admin.issue_preview', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/issue', 'SelectController@selectIssue')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/insert/issue', 'InsertController@insertIssue')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/issue', 'UpdateController@updateIssue')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/issue_alumni', 'UpdateController@updateIssueAlumni')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/delete/issue', 'DeleteController@deleteIssue')->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/generate/{lang}/{id}',[
    'uses' => 'GenerateController@download',
    'as'   => 'switch'
])->middleware('auth')->middleware('inGroup:2');


#Category****************************************************************************************
Route::get('/admin/category', function () {
    return view('admin.category');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/category/new', function () {
  $data = [];
  $data['category_types'] = DB::table('category_type')->where('deletable', '1')->get();
  return view('admin.category_new', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/category/{id}', function ($id) {
  $data = [];
  $data['id'] = $id;
  $data['category'] = DB::table('category')->select('category.*', 'category_type.id AS category_type_id', 'category_type.deletable')->join('category_type', 'category_type.id', '=', 'category.type')->where('category.id', $id)->first();
  $data['category_types'] = DB::table('category_type')->where('deletable', '1')->get();
  return view('admin.category_edit', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/category', 'SelectController@selectCategory')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/insert/category', 'InsertController@insertCategory')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/category', 'UpdateController@updateCategory')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/delete/category', 'DeleteController@deleteCategory')->middleware('auth')->middleware('inGroup:2');


#Passage****************************************************************************************
Route::get('/admin/passage', function () {
    return view('admin.passage');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/passage/new/{issue_id?}/{category_id?}', function ($issue_id = null, $category_id = null) {
  $data = [];
  $data['issues'] = DB::table('issue')->orderBy('number', 'DESC')->get()->toArray();
  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();
  $data['issue_id'] = $issue_id;
  $data['category_id'] = $category_id;

  if($category_id != null){
    $data['category_type'] = DB::table('category')->join('category_type', 'category_type.id', '=', 'category.type')->where('category.id', $category_id)->first();
  }else{
    $data['category_type'] = null;
  }

  return view('admin.passage_new', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/passage/{id}', function ($id) {
  $data = [];
  $data['issues'] = DB::table('issue')->orderBy('number', 'DESC')->get()->toArray();
  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();
  $data['passage'] = DB::table('passage')->select('passage.*', 'category.type', 'users.name')->join('category', 'category.id', '=', 'passage.category_id')->leftJoin('users', 'users.id', '=', 'passage.publish_by')->where('passage.id', $id)->first();
  return view('admin.passage_edit', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/passage', 'SelectController@selectPassage')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/insert/passage', 'InsertController@insertPassage')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/passage', 'UpdateController@updatePassage')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/delete/passage', 'DeleteController@deletePassage')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/sequence', 'UpdateController@updateSequence')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/cover', 'UpdateController@updateCover')->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/passage/{issue_id}/{category_id}',[
    'uses' => 'SelectController@selectPassageBy',
    'as'   => 'switch'
])->middleware('auth')->middleware('inGroup:2');

#Backend Preview Passage****************************************************************************************
Route::get('/{locale}/preview/issue/{issue_date}/{issue_id?}', function ($locale, $issue_date, $issue_id = '') {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $issue = DB::table('issue')->where('date', $issue_date)->orderBy('id', 'desc')->first();

  if($issue == null){
    return view('index');
  }

  $data = [];
  $data['lang'] = $locale;

  $data['issues'] = DB::table('issue')->where('id', '!=', $issue->id)->orderBy('date', 'desc')->get()->toArray();

  $data['passage'] = DB::table('passage')->where('id', $issue_id)->first();

  $data['issue'] = $issue;

  $data['path'] = '/issue/' . $issue_date . '/' . $issue_id;

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('admin.passage_preview', $data);
});

#Banner****************************************************************************************
Route::get('/admin/banner', function () {
    return view('admin.banner');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/banner/new', function () {
  return view('admin.banner_new');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/banner/{id}', function ($id) {
  $data = [];
  $data['id'] = $id;
  $data['banner'] = DB::table('banner')->where('id', $id)->first();
  return view('admin.banner_edit', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/banner', 'SelectController@selectBanner')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/insert/banner', 'InsertController@insertBanner')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/banner', 'UpdateController@updateBanner')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/delete/banner', 'DeleteController@deleteBanner')->middleware('auth')->middleware('inGroup:2');


Route::post('/admin/action/upload/images', 'UploadController@storeImages')->middleware('auth');

#Buttons****************************************************************************************
Route::get('/admin/button', function () {
    return view('admin.button');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/button/{id}', function ($id) {
  $data = [];
  $data['button'] = DB::table('buttons')->where('id', $id)->first();
  return view('admin.button_edit', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/button/{type}', function ($type) {
  $buttons = [];
  $buttons = DB::table('buttons')->where('type', $type)->get();

  foreach($buttons as &$button){
    $button->icon_zh = '<img src="/images/icon/' . $button->icon_zh . '" height="" width="50"/>';
    $button->icon_en = '<img src="/images/icon/' . $button->icon_en . '" height="" width="50"/>';
    $button->title_zh = htmlspecialchars($button->title_zh);
    $button->title_en = htmlspecialchars($button->title_en);
  };

  return \Response::json($buttons);
})->middleware('auth')->middleware('inGroup:2');

Route::post('/admin/action/update/button', 'UpdateController@updateButton')->middleware('auth')->middleware('inGroup:2');

#Users****************************************************************************************
Route::get('/admin/users', function () {
    return view('admin.users');
})->middleware('auth')->middleware('inGroup:10');

Route::get('/admin/users/new', function () {
  return view('admin.users_new');
})->middleware('auth')->middleware('inGroup:10');

Route::get('/admin/users/{id}', function ($id) {
  $data = [];
  $data['id'] = $id;
  $data['user'] = DB::table('users')->where('id', $id)->first();
  return view('admin.users_edit', $data);
})->middleware('auth')->middleware('inGroup:10');

Route::get('/admin/action/select/users', 'SelectController@selectUsers')->middleware('auth')->middleware('inGroup:10');
Route::post('/admin/action/insert/users', 'InsertController@insertUsers')->middleware('auth')->middleware('inGroup:10');
Route::post('/admin/action/update/users', 'UpdateController@updateUsers')->middleware('auth')->middleware('inGroup:10');
Route::post('/admin/action/delete/users', 'DeleteController@deleteUsers')->middleware('auth')->middleware('inGroup:10');

#Preview News****************************************************************************************
Route::get('/{locale}/preview/news/{approve_id}', function ($locale, $approve_id) {

  if ($locale != 'zh-hk' && $locale != 'en') {
    return redirect('/');
  }

  session(['lang' => $locale]);
  App::setLocale($locale);

  $data = [];
  $data['lang'] = $locale;

  $data['passage'] = DB::table('passage')->where('approve_id', $approve_id)->first();

  if($data['passage'] == null){
    return view('index');
  }

  $data['buttons'] = DB::table('buttons')->get()->toArray();

  $data['host'] = Request::getHost();

  return view('news_preview', $data);
});

#Approve****************************************************************************************
Route::get('/admin/approve', function () {
    return view('admin.approve');
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/approve/{id}', function ($id) {
  $data = [];
  $data['id'] = $id;
  $data['issues'] = DB::table('issue')->orderBy('number', 'DESC')->get()->toArray();
  $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();
  $data['passage'] = DB::table('passage')->select('passage.*', 'category.type')->join('category', 'category.id', '=', 'passage.category_id')->where('passage.id', $id)->first();
  return view('admin.approve_view', $data);
})->middleware('auth')->middleware('inGroup:2');

Route::get('/admin/action/select/approve', 'SelectController@selectApprove')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/update/approve', 'UpdateController@updateApprove')->middleware('auth')->middleware('inGroup:2');
Route::post('/admin/action/delete/approve', 'DeleteController@deleteApprove')->middleware('auth')->middleware('inGroup:2');


#Master
Auth::routes();

Route::post('/admin/login', [ 'as' => '/admin/login', 'uses' => 'LoginController@do']);
Route::get('/admin/home', 'HomeController@index')->name('home');
