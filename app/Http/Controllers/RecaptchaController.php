<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecaptchaController extends Controller
{

  public function check(Request $request)
  {
    $captcha = $request->input("token");
    $secretKey = '6LdXH90UAAAAAN_ifdCb6PK41jNsIMnjhd2Gpjgv';

    $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) . '&response=' . urlencode($captcha);
    $response = file_get_contents($url);
    $responseKeys = json_decode($response, true);
    if ($responseKeys['score'] >= 0.5) {
       return "true";
    } else {
       return "false";
    }

  }

}
