<?php

namespace App\Http\Controllers;

use App;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\File;
use Fusonic\OpenGraph\Consumer;
use DateTime;

class GenerateController extends Controller
{

  public function download($lang, $id)
  {
    if($lang != "zh-hk" && $lang != "en"){
      return;
    }

    if($id == null || $id == ""){
      return;
    }

    App::setLocale($lang);

    $issue = DB::table('issue')->where('id', $id)->first();

    $data = [];

    $data['cover_news'] = DB::table('passage')
      ->where('passage_publish', '1')
      ->where('issue_id', $issue->id)
      ->where('is_cover', 1)
      ->first();

    $data['categories'] = DB::table('category')->orderBy('sequence', 'ASC')->get()->toArray();

    foreach ($data['categories'] as $category) {
      if($category->type == 3 && $issue->alumni_link != ''){
        $process_link = explode("/", $issue->alumni_link);
        $link_lang = '';

        if($process_link[2] == "www.cuhk.edu.hk" && ($process_link[3] == "chinese" || $process_link[3] == "english")){
          if ($lang == 'zh-hk') {
            $process_link[3] = "chinese";
          }else if($lang == 'en'){
            $process_link[3] = "english";
          }
        }

        $after_link = implode('/', $process_link);

        //$category->alumni = OpenGraph::fetch($after_link);
        $consumer = new Consumer();
        $category->alumni = $consumer->loadUrl($after_link);
      }

      $category->passages = DB::table('passage')
      ->join('issue', 'issue.id', '=', 'passage.issue_id')
      ->join('category', 'category.id', '=', 'passage.category_id')
      ->select('passage.*', 'issue.date', 'category.name_zh')
      ->where('passage.passage_publish', '1')
      ->where('issue.id', $issue->id)
      ->where('passage.category_id', $category->id)
      ->orderBy(($category->type == 4 ? 'passage.event_date' : 'passage.sequence'), 'ASC')
      ->orderBy('passage.create_date', 'ASC')
      ->get();
    }



    $data['issue'] = $issue;

    $data['banner'] = DB::table('banner')
    ->where('publish', '1')
    ->where('start_date', '<=', date("Y-m-d"))
    ->where('end_date', '>=', date("Y-m-d"))
    ->first();

    $data['buttons'] = DB::table('buttons')->get()->toArray();

    $filename = "eDM_" . $issue->number . "_" . $lang . ".html";

    $data['domain'] =  Request::server("HTTP_HOST");
    $data['protocol'] = Request::secure() ? "https" : "http";

    //return \View::make('admin.download')->with('data', $data)->with('lang', $lang);

    $content = \View::make('admin.download')->with('data', $data)->with('lang', $lang);
    return \Response::make($content, '200')->header('Content-Type', 'text/html')->header('Content-disposition', 'attachment; filename="'.$filename.'"');
  }
}
