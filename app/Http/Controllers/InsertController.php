<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Facades\Input;

class InsertController extends Controller
{

  public function insertIssue(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'number' => 'required|integer',
        'date' => 'required|date',
        'alumni_link' => 'nullable|url',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    try {
      DB::table('issue')->insert(
        [
          'number' => $request->input("number"),
          'date' => $request->input("date"),
          'alumni_link' => $request->input("alumni_link"),
        ]
      );
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues')->with('alert-success', 'New issue has been added!');
  }

  public function insertCategory(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'name_zh' => 'required',
        'name_en' => 'required',
        'icon' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'type' => 'required|integer',
        'col_no' => 'nullable|integer|between:1,4',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    if($request->input("type") == 1 && $request->input("col_no") == null){
      Input::flash();
      return redirect()->back()->with('alert-error', 'The column number cannot be empty.');
    }

    $data = array(
      'name_zh' => $request->input("name_zh"),
      'name_en' => $request->input("name_en"),
      'type' =>  $request->input("type"),
    );

    if($request->hasfile('icon'))
    {
      if ($request->file('icon')->isValid())
      {
        $file = $request->file('icon');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/category/', $name);

        $data['icon'] = $name;
      }
    }

    if($request->input("col_no") != null){
      $data['col_no'] = $request->input("col_no");
    }

    $insert_id = null;

    try {
      $insert_id = DB::table('category')->insertGetId($data);
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    try {
      DB::table('category')
          ->where('id', $insert_id)
          ->update([
            'sequence' => $insert_id
          ]);
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }



    return redirect('/admin/category')->with('alert-success', 'New category has been added!');
  }

  public function insertPassage(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'issue_id' => 'required|integer',
        'category_id' => 'required|integer',
        'title_zh' => 'required',
        'title_en' => 'required',
        'link_zh' => 'nullable|url',
        'link_en' => 'nullable|url',
        'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'event_date' => 'nullable|date',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $catagory_type = DB::table('category')->where('id', $request->input("category_id"))->first()->type;

    if($catagory_type == 4 && $request->input("event_date") == null){
      Input::flash();
      $request->session()->put('_old_input.category_type', $catagory_type);
      return redirect()->back()->with('alert-error', 'The event date cannot be empty.');
    }

    $data = array(
      'issue_id' => $request->input("issue_id"),
      'category_id' => $request->input("category_id"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'content_zh' => $request->input("content_zh"),
      'content_en' => $request->input("content_en"),
      'passage_publish' => $request->input("passage_publish") == null ? 0 : 1,
      'publish_by' => Auth::id(),
      'approve_id' => self::GenerateID(),
      'approved' => 1,
      'approved_by' => Auth::id(),
    );

    if($request->hasfile('cover'))
    {
      if ($request->file('cover')->isValid())
      {
        $file = $request->file('cover');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/blog/', $name);

        $data['cover'] = $name;
      }
    }

    if($request->input("event_date") != null){
      $data['event_date'] = $request->input("event_date");
    }

    try {
      DB::table('passage')->insert($data);
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues/' . $request->input("issue_id"))->with('alert-success', 'New category has been added!');
  }

  public function insertBanner(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'ads_title' => 'required',
        'link_zh' => 'required|url',
        'link_en' => 'required|url',
        'ads_pc_zh' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_mobile_zh' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_pc_en' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_mobile_en' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after:start_date',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $banners = DB::table('banner')->get();

    $overlap = false;
    $overlap_banner = null;
    foreach($banners as $banner){
      $overlap_banner = $banner;
      if(strtotime($request->input("start_date")) <= strtotime($banner->start_date) && strtotime($banner->start_date) <= strtotime($request->input("end_date"))){
        $overlap = true;
        break;
      }
      if(strtotime($banner->start_date) <= strtotime($request->input("start_date")) && strtotime($request->input("end_date")) <= strtotime($banner->end_date)){
        $overlap = true;
        break;
      }
      if(strtotime($request->input("start_date")) <= strtotime($banner->end_date) && strtotime($banner->end_date) <= strtotime($request->input("end_date"))){
        $overlap = true;
        break;
      }
    }

    if($overlap){
      Input::flash();
      return redirect()->back()->with('alert-error', "The Date Range is overlapped with another banners (" . $overlap_banner->ads_title . ": " . $overlap_banner->start_date . " to " . $overlap_banner->end_date . ").");
    }

    $ads_pc_zh = '';
    $ads_mobile_zh = '';
    $ads_pc_en = '';
    $ads_mobile_en = '';


    if($request->hasfile('ads_pc_zh'))
    {

      if ($request->file('ads_pc_zh')->isValid())
      {

        $file = $request->file('ads_pc_zh');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $ads_pc_zh = $name;
      }
    }


    if($request->hasfile('ads_mobile_zh'))
    {

      if ($request->file('ads_mobile_zh')->isValid())
      {
        $file = $request->file('ads_mobile_zh');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $ads_mobile_zh = $name;
      }
    }

    if($request->hasfile('ads_pc_en'))
    {

      if ($request->file('ads_pc_en')->isValid())
      {

        $file = $request->file('ads_pc_en');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $ads_pc_en = $name;
      }
    }


    if($request->hasfile('ads_mobile_en'))
    {

      if ($request->file('ads_mobile_en')->isValid())
      {
        $file = $request->file('ads_mobile_en');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $ads_mobile_en = $name;
      }
    }

    try {
      DB::table('banner')->insert(
        [
          'ads_title' => $request->input("ads_title"),
          'link_zh' => $request->input("link_zh"),
          'link_en' => $request->input("link_en"),
          'ads_pc_zh' => $ads_pc_zh,
          'ads_mobile_zh' => $ads_mobile_zh,
          'ads_pc_en' => $ads_pc_en,
          'ads_mobile_en' => $ads_mobile_en,
          'start_date' => $request->input("start_date"),
          'end_date' => $request->input("end_date"),
          'publish' => $request->input("publish") == null ? 0 : 1,
        ]
      );
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/banner')->with('alert-success', 'New banner has been added!');
  }

  public function insertUsers(Request $request)
  {

    $user = Auth::user();

    $validator = Validator::make($request->all(), [
        'name' => 'required|between:2,255',
        'email' => 'required|email',
        'password' => 'required|between:4,255',
        'confirm_password' => 'required|same:password',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    try {
      $user = new User;
      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->group = 2;
      $user->password = Hash::make($request->input('password'));
      $user->save();
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/users')->with('alert-success', 'New user has been added!');
  }

  public function insertNews(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'publisher' => 'required|between:2,32',
        'publisher_organizer' => 'required|between:3,100',
        'publisher_email' => 'required|email',
        'publisher_phone' => 'required|regex:/[0-9 +()]+/|between:8,30',
        'category_id' => 'required|integer',
        'title_zh' => 'required',
        'title_en' => 'required',
        'link_zh' => 'nullable|url',
        'link_en' => 'nullable|url',
        'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'event_date' => 'nullable|date',
    ]);

    if ($validator->fails()) {
      Input::flash();
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    if($request->input("category_id") == 3){
      Input::flash();
      return redirect()->back()->with('alert-error', "Please submit event in http://alumni.cuhk.edu.hk/en/eventcalendar/webform.");
    }

    $content_zh = str_replace("<p><br></p>", "", $request->input("content_zh"));
    $content_en = str_replace("<p><br></p>", "", $request->input("content_en"));

    if($request->input("link_zh") == "" && $request->input("link_en") == "" && $content_zh == "" && $content_en == ""){
      Input::flash();
      return redirect()->back()->with('alert-error', "You must fill in Chinese Link - English Link Or Chinese Content - English Content");
    }

    if(($request->input("link_zh") != "" || $request->input("link_en") != "") && ($content_zh != "" || $content_zh != "")){
      Input::flash();
      return redirect()->back()->with('alert-error', "You cannot fill in Chinese Link - English Link And Chinese Content - English Content together.");
    }

    if(($request->input("link_zh") == "" && $request->input("link_en") != "") || ($request->input("link_zh") != "" &&  $request->input("link_en") == "")){
      Input::flash();
      return redirect()->back()->with('alert-error', "You must fill in both Chinese and English Link.");
    }

    if(($content_zh == "" && $content_en != "") || ($content_zh != "" && $content_en == "")){
      Input::flash();
      return redirect()->back()->with('alert-error', "You must fill in both Chinese and English Content.");
    }

    $catagory_type = DB::table('category')->where('id', $request->input("category_id"))->first()->type;

    if($catagory_type == 4 && $request->input("event_date") == null){
      Input::flash();
      $request->session()->put('_old_input.category_type', $catagory_type);
      return redirect()->back()->with('alert-error', 'The event date cannot be empty.');
    }

    $data = array(
      'category_id' => $request->input("category_id"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'content_zh' => $request->input("content_zh"),
      'content_en' => $request->input("content_en"),
      'passage_publish' => $request->input("passage_publish") == null ? 0 : 1,
      //'publish_by' => Auth::id(),
      'publisher' => $request->input("publisher"),
      'publisher_organizer' => $request->input("publisher_organizer"),
      'publisher_email' => $request->input("publisher_email"),
      'publisher_phone' => $request->input("publisher_phone"),
      'approve_id' => self::GenerateID(),
      'approved' => 0,
    );

    if($request->hasfile('cover'))
    {
      if ($request->file('cover')->isValid())
      {
        $file = $request->file('cover');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/blog/', $name);

        $data['cover'] = $name;
      }
    }

    if($request->input("event_date") != null){
      $data['event_date'] = $request->input("event_date");
    }

    try {
      DB::table('passage')->insert($data);
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/news/success');
    //return redirect('/admin/news/' . $request->input("issue_id"))->with('alert-success', 'News has been added!');
  }

  public static function GenerateID($length = 10)
  {
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
  }

}
