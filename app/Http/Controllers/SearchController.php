<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use App;

class SearchController extends Controller
{

  public function searchPassage(Request $request, $locale)
  {
    $keywords = $request->input("keywords");

    if($keywords == ''){
      return;
    }

    $length = mb_strlen($keywords);

    if($locale == "zh-hk"){

      $passages = DB::table('passage')
      ->join('issue', 'issue.id', '=', 'passage.issue_id')
      ->join('category', 'category.id', '=', 'passage.category_id')
      ->select('passage.*', 'issue.number', 'issue.date', 'category.name_zh')
      ->where('issue.publish', 1)
      ->where('passage.passage_publish', 1)
      ->where(function ($query) use ($keywords) {
          $query->where('passage.title_zh', 'LIKE', "%{$keywords}%")
                ->orWhere('passage.content_zh', 'LIKE', "%{$keywords}%");
      })
      ->orderBy('create_date', 'DESC')
      ->get();

      foreach($passages as $passage){
        $passage->name_zh = htmlspecialchars($passage->name_zh);
        $passage->title_zh = htmlspecialchars($passage->title_zh);
        $no_tags_zh = strip_tags($passage->content_zh);
        $length_zh = mb_strlen($no_tags_zh);
        $position_zh = mb_strpos($no_tags_zh, $keywords);

        if($position_zh === false){
          $passage->search_zh = str_limit(strip_tags($no_tags_zh), 150, '...');
        }else{
          $head_length = 60;
          $tail_length = 60;

          $head_point = "...";
          $tail_point = "...";

          if($position_zh - $head_length < 0){
            $head_length = $position_zh;
            $head_point = "";
          }

          if($position_zh + $length - 1 + $tail_length > $length_zh){
            $tail_length = $length_zh - ($position_zh + $length);
            $tail_point = "";
          }

          $head_zh = mb_substr($no_tags_zh, $position_zh - $head_length, $head_length);
          $tail_zh = mb_substr($no_tags_zh, $position_zh + $length, $tail_length);

          $passage->search_zh = $head_point . $head_zh . "<span class='highlight'>" . $keywords . "</span>" . $tail_zh . $tail_point;
        }

      }

    }else if($locale == "en"){

      $passages = DB::table('passage')
      ->join('issue', 'issue.id', '=', 'passage.issue_id')
      ->join('category', 'category.id', '=', 'passage.category_id')
      ->select('passage.*', 'issue.number', 'issue.date', 'category.name_en')
      ->where('issue.publish', 1)
      ->where('passage.passage_publish', 1)
      ->where(function ($query) use ($keywords) {
          $query->where('passage.title_en', 'LIKE', "%{$keywords}%")
                ->orWhere('passage.content_en', 'LIKE', "%{$keywords}%");
      })
      ->orderBy('create_date', 'DESC')
      ->get();

      foreach($passages as $passage){
        $passage->name_en = htmlspecialchars($passage->name_en);
        $passage->title_en = htmlspecialchars($passage->title_en);
        $no_tags_en = strip_tags($passage->content_en);
        $length_en = mb_strlen($no_tags_en);
        $position_en = mb_strpos($no_tags_en, $keywords);

        if($position_en === false){
          $passage->search_en = str_limit(strip_tags($no_tags_en), 150, '...');
        }else{
          $head_length = 60;
          $tail_length = 60;

          $head_point = "...";
          $tail_point = "...";

          if($position_en - $head_length < 0){
            $head_length = $position_en;
            $head_point = "";
          }

          if($position_en + $length - 1 + $tail_length > $length_en){
            $tail_length = $length_en - ($position_en + $length);
            $tail_point = "";
          }

          $head_en = mb_substr($no_tags_en, $position_en - $head_length, $head_length);
          $tail_en = mb_substr($no_tags_en, $position_en + $length, $tail_length);

          $passage->search_en = $head_point . $head_en . "<span class='highlight'>" . $keywords . "</span>" . $tail_en . $tail_point;
        }


      }
    }

    return \Response::json($passages, 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_PRETTY_PRINT);
  }

}
