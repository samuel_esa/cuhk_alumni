<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class SelectController extends Controller
{

  public function selectIssue(Request $request)
  {
    $issues = DB::table('issue')->get();

    foreach($issues as &$issue){
      $issue->publish = $issue->publish == 1 ? "Yes" : "-";
    };

    return \Response::json($issues);
  }

  public function selectCategory(Request $request)
  {
    $categorys = DB::table('category')
    ->select('category.*', 'category_type.name')
    ->join('category_type', 'category_type.id', '=', 'category.type')
    ->orderBy('sequence', 'ASC')
    ->get();

    foreach($categorys as &$category){
      $category->name_zh = htmlspecialchars($category->name_zh);
      $category->name_en = htmlspecialchars($category->name_en);
    };

    return \Response::json($categorys);
  }

  public function selectPassage(Request $request)
  {
    $passages = DB::table('passage')
    ->join('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->leftJoin('users', 'users.id', '=', 'passage.publish_by')
    ->select('passage.*', 'issue.number', 'issue.date', 'category.name_zh', 'users.name')
    ->orderBy('create_date', 'DESC')
    ->get();

    foreach($passages as &$passage){
      $passage->passage_publish = $passage->passage_publish == 1 ? '<span class="badge badge-success">Yes</span>' : "-";
      if($passage->name == null){
        $passage->name = htmlspecialchars($passage->publisher);
      }else{
        $passage->name = htmlspecialchars($passage->name);
      }

      $passage->name_zh = htmlspecialchars($passage->name_zh);
      //$passage->name_en = htmlspecialchars($passage->name_en);
      $passage->title_zh = htmlspecialchars($passage->title_zh);
      $passage->title_en = htmlspecialchars($passage->title_en);
      $passage->content_zh = clean($passage->content_zh);
      $passage->content_en = clean($passage->content_en);
    };

    return \Response::json($passages);
  }

  public function selectPassageBy($issue_id, $category_id)
  {

    $category_type = DB::table('category')
    ->join('category_type', 'category_type.id', '=', 'category.type')
    ->where('category.id', $category_id)
    ->first();

    $col = '';

    if($category_type->id == 4){
      $col = 'event_date';
    }else{
      $col = 'sequence';
    }

    $passages = DB::table('passage')
    ->join('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->leftJoin('users', 'users.id', '=', 'passage.publish_by')
    ->select('passage.*', 'issue.number', 'issue.date', 'category.name_zh', 'users.name')
    ->where("issue_id", $issue_id)
    ->where("category_id", $category_id)
    ->orderBy($col, 'ASC')
    ->orderBy('create_date', 'DESC')
    ->get();

    foreach($passages as &$passage){
      $passage->passage_publish = $passage->passage_publish == 1 ? '<span class="badge badge-success">Yes</span>' : "-";
      if($passage->name == null){
        $passage->name = htmlspecialchars($passage->publisher);
      }else{
        $passage->name = htmlspecialchars($passage->name);
      }

      $passage->name_zh = htmlspecialchars($passage->name_zh);
      //$passage->name_en = htmlspecialchars($passage->name_en);
      $passage->title_zh = htmlspecialchars($passage->title_zh);
      $passage->title_en = htmlspecialchars($passage->title_en);
      $passage->content_zh = clean($passage->content_zh);
      $passage->content_en = clean($passage->content_en);
    };

    return \Response::json($passages);
  }

  public function selectBanner(Request $request)
  {
    $banners = DB::table('banner')->orderBy('end_date', 'DESC')->get();

    foreach($banners as &$banner){
      $banner->publish = $banner->publish == 1 ? "Yes" : "-";
      $banner->ads_title = htmlspecialchars($banner->ads_title);
    };

    return \Response::json($banners);
  }

  public function selectUsers(Request $request)
  {
    $users = DB::table('users')->get();

    foreach($users as &$user){
      if($user->group == 1){
        $user->group = '<span class="badge badge-secondary">Users</span>';
      }else if($user->group == 2){
        $user->group = '<span class="badge badge-info">Users</span>';
      }else if($user->group == 10){
        $user->group = '<span class="badge badge-warning">Admin</span>';
      }

      $user->name = htmlspecialchars($user->name);
    };

    return \Response::json($users);
  }

  public function selectNews(Request $request)
  {
    $user = Auth::user();
    $passages = DB::table('passage')
    ->leftJoin('issue', 'issue.id', '=', 'passage.issue_id')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->select('passage.*', 'issue.number', 'issue.date', 'category.name_zh')
    ->where('publish_by', $user->id)
    ->orderBy('create_date', 'DESC')
    ->get();

    foreach($passages as &$passage){
      $passage->approved = $passage->approved == 1 ? '<span class="badge badge-success">Yes</span>' : "-";

      $passage->name_zh = htmlspecialchars($passage->name_zh);
      //$passage->name_en = htmlspecialchars($passage->name_en);
      $passage->title_zh = htmlspecialchars($passage->title_zh);
      $passage->title_en = htmlspecialchars($passage->title_en);
      $passage->content_zh = clean($passage->content_zh);
      $passage->content_en = clean($passage->content_en);
    };

    return \Response::json($passages);
  }

  public function selectApprove(Request $request)
  {
    $passages = DB::table('passage')
    ->select('passage.*', 'category.name_zh')
    ->join('category', 'category.id', '=', 'passage.category_id')
    ->where('passage.approved', 0)
    ->orderBy('create_date', 'DESC')
    ->get();

    foreach($passages as &$passage){
      $passage->publisher = htmlspecialchars($passage->publisher);
      $passage->publisher_organizer = htmlspecialchars($passage->publisher_organizer);

      $passage->name_zh = htmlspecialchars($passage->name_zh);
      //$passage->name_en = htmlspecialchars($passage->name_en);
      $passage->title_zh = htmlspecialchars($passage->title_zh);
      $passage->title_en = htmlspecialchars($passage->title_en);
      $passage->content_zh = clean($passage->content_zh);
      $passage->content_en = clean($passage->content_en);
    };

    return \Response::json($passages);
  }

}
