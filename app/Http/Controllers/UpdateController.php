<?php

namespace App\Http\Controllers;

use Validator;
use Session;
use View;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class UpdateController extends Controller
{

  public function updateIssue(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'number' => 'required|integer',
        'date' => 'required|date',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $data = array(
      'number' => $request->input("number"),
      'date' => $request->input("date"),
      'publish' => $request->input("publish") == null ? 0 : 1
    );

    try {
      DB::table('issue')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues/' . $request->input("id"))->with('alert-success', 'Issue has been updated!');
  }

  public function updateIssueAlumni(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'alumni_link' => 'nullable|url',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $data = array(
      'alumni_link' => $request->input("alumni_link"),
    );

    try {
      DB::table('issue')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues/' . $request->input("id"))->with('alert-success', 'Issue Alumni has been updated!');
  }

  public function updateCategory(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'name_zh' => 'required',
        'name_en' => 'required',
        'icon' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'type' => 'nullable|integer',
        'col_no' => 'nullable|integer|between:1,4'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    if($request->input("type") == 1 && $request->input("col_no") == null){
      return redirect()->back()->with('alert-error', 'The column number cannot be empty.');
    }

    $data = array(
      'name_zh' => $request->input("name_zh"),
      'name_en' => $request->input("name_en")
    );

    $category = DB::table('category')
        ->where('id', $request->input("id"))
        ->first();

    if($category->type != 3){
      $data['type'] = $request->input("type");
    }

    if($request->hasfile('icon'))
    {
      if ($request->file('icon')->isValid())
      {
        $file = $request->file('icon');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/category/', $name);

        $data['icon'] = $name;
      }
    }else{
      if($request->input("remove_img") == 1){
        $data['icon'] = '';
      }
    }

    if($request->input("col_no") != null){
      $data['col_no'] = $request->input("col_no");
    }

    try {
      DB::table('category')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/category')->with('alert-success', 'Category has been updated!');
  }

  public function updatePassage(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'issue_id' => 'required|integer',
        'category_id' => 'required|integer',
        'title_zh' => 'required',
        'title_en' => 'required',
        'link_zh' => 'nullable|url',
        'link_en' => 'nullable|url',
        'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'event_date' => 'nullable|date'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $catagory_type = DB::table('category')->where('id', $request->input("category_id"))->first()->type;

    if($catagory_type == 4 && $request->input("event_date") == null){
      return redirect()->back()->with('alert-error', 'The event date cannot be empty.');
    }

    $data = array(
      'issue_id' => $request->input("issue_id"),
      'category_id' => $request->input("category_id"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'content_zh' => $request->input("content_zh"),
      'content_en' => $request->input("content_en"),
      'passage_publish' => $request->input("passage_publish") == null ? 0 : 1
    );

    if($request->hasfile('cover'))
    {
      if ($request->file('cover')->isValid())
      {

        $file = $request->file('cover');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/blog/', $name);

        $data['cover'] = $name;
      }
    }else{
      if($request->input("remove_img") == 1){
        $data['cover'] = '';
      }
    }

    if($request->input("event_date") != null){
      $data['event_date'] = $request->input("event_date");
    }

    try {
      DB::table('passage')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues/' . $request->input("issue_id"))->with('alert-success', 'Passage has been updated!');
  }

  public function updateSequence(Request $request)
  {

     $data = array(
      $request->input("col") => $request->input("sequence"),
    );

    try {
      DB::table($request->input("table"))
        ->where('id', $request->input("id"))
        ->update($data);
        Session::flash('success', 'The sequence has been updated!');
        return View::make('partials/flash-messages');
    }
    catch(QueryException $e) {
      Session::flash('error', $e->getMessage());
      return View::make('partials/flash-messages');
    }
  }

  public function updateCover(Request $request)
  {

     $id = $request->input("id");

     $record = DB::table('passage')
       ->where('id', $id)
       ->first();

    $issue_id = $record->issue_id;

    DB::table('passage')
      ->where('issue_id', $issue_id)
      ->update([
        'is_cover' => 0
      ]);

    try {
      DB::table('passage')
        ->where('id', $id)
        ->update([
          'is_cover' => 1
        ]);
        Session::flash('success', 'The cover has been changed!');
        return View::make('partials/flash-messages');
    }
    catch(QueryException $e) {
      Session::flash('error', $e->getMessage());
      return View::make('partials/flash-messages');
    }
  }

  public function updateBanner(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'ads_title' => 'required',
        'link_zh' => 'required|url',
        'link_en' => 'required|url',
        'ads_pc_zh' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_mobile_zh' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_pc_en' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'ads_mobile_en' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'start_date' => 'required|date',
        'end_date' => 'required|date|after:start_date',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $banners = DB::table('banner')->get();

    $overlap = false;
    $overlap_banner = null;

    foreach($banners as $banner){

      if($banner->id == $request->input("id")){
        continue;
      }

      $overlap_banner = $banner;
      if(strtotime($request->input("start_date")) <= strtotime($banner->start_date) && strtotime($banner->start_date) <= strtotime($request->input("end_date"))){
        $overlap = true;
        break;
      }
      if(strtotime($banner->start_date) <= strtotime($request->input("start_date")) && strtotime($request->input("end_date")) <= strtotime($banner->end_date)){
        $overlap = true;
        break;
      }
      if(strtotime($request->input("start_date")) <= strtotime($banner->end_date) && strtotime($banner->end_date) <= strtotime($request->input("end_date"))){
        $overlap = true;
        break;
      }
    }

    if($overlap){
      return redirect()->back()->with('alert-error', "The Date Range is overlapped with another banners (" . $overlap_banner->ads_title . ": " . $overlap_banner->start_date . " to " . $overlap_banner->end_date . ").");
    }

    $data = array(
      'ads_title' => $request->input("ads_title"),
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'start_date' => $request->input("start_date"),
      'end_date' => $request->input("end_date"),
      'publish' => $request->input("publish") == null ? 0 : 1,
    );

    if($request->hasfile('ads_pc_zh'))
    {
      if ($request->file('ads_pc_zh')->isValid())
      {
        $file = $request->file('ads_pc_zh');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $data['ads_pc_zh'] = $name;
      }
    }

    if($request->hasfile('ads_mobile_zh'))
    {
      if ($request->file('ads_mobile_zh')->isValid())
      {
        $file = $request->file('ads_mobile_zh');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $data['ads_mobile_zh'] = $name;
      }
    }

    if($request->hasfile('ads_pc_en'))
    {
      if ($request->file('ads_pc_en')->isValid())
      {
        $file = $request->file('ads_pc_en');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $data['ads_pc_en'] = $name;
      }
    }

    if($request->hasfile('ads_mobile_en'))
    {
      if ($request->file('ads_mobile_en')->isValid())
      {
        $file = $request->file('ads_mobile_en');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/banner/', $name);
        $data['ads_mobile_en'] = $name;
      }
    }

    if($request->input("remove_pc_zh") == 1){
      $data['ads_pc_zh'] = '';
    }

    if($request->input("remove_mobile_zh") == 1){
      $data['ads_mobile_zh'] = '';
    }

    if($request->input("remove_pc_en") == 1){
      $data['ads_pc_en'] = '';
    }

    if($request->input("remove_mobile_en") == 1){
      $data['ads_mobile_en'] = '';
    }

    try {
      DB::table('banner')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/banner')->with('alert-success', 'Banner has been updated!');
  }

  public function updateButton(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'icon_zh' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'icon_en' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'link_zh' => 'required|url',
        'link_en' => 'nullable|url',
        'title_zh' => 'nullable',
        'title_en' => 'nullable',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $data = array(
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
    );

    if($request->hasfile('icon_zh'))
    {
      if ($request->file('icon_zh')->isValid())
      {
        $file = $request->file('icon_zh');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/icon/', $name);
        $data['icon_zh'] = $name;
      }
    }

    if($request->hasfile('icon_en'))
    {
      if ($request->file('icon_en')->isValid())
      {
        $file = $request->file('icon_en');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/icon/', $name);
        $data['icon_en'] = $name;
      }
    }

    $type = DB::table('buttons')
      ->where('id', $request->input("id"))
      ->first()->type;

    if($type == 1 && ($request->input("remove_zh") == 1 || $request->input("remove_en") == 1)){
      return redirect()->back()->with('alert-error', "Please upload all icons for this button!");
    }

    if($type == 2 && $request->input("remove_zh") == 1){
      return redirect()->back()->with('alert-error', "Icon is missing for this button!");
    }

    if($type == 3 && $request->input("remove_zh") == 1){
      return redirect()->back()->with('alert-error', "Icon is missing for this button!");
    }

    try {
      DB::table('buttons')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/button')->with('alert-success', 'Button has been updated!');
  }

  public function updateUsers(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'id' => 'required|integer',
        'name' => 'required|between:2,255',
        'email' => 'required|email',
        'password' => 'nullable|between:4,255',
        'confirm_password' => 'nullable|same:password',
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    try {
      $user = User::where('id', $request->input('id'))->first();
      $user->name = $request->input('name');
      $user->email = $request->input('email');

      /*
      if($request->input('group') != null && $user->group == 10 && $request->input('group') < 10){
        return redirect()->back()->with('alert-error', 'You cannot change Admin\'s User group.');
      }else if($request->input('group') != null && $user->group != 10 && $request->input('group') == 10){
        return redirect()->back()->with('alert-error', 'Only one Admin User is allowed.');
      }else if($request->input('group') != null && $request->input('group') != ''){
        $user->group = $request->input('group');
      }
      */

      if($request->input('password') != ''){
        $user->password = Hash::make($request->input('password'));
      }
      $user->save();
    }
    catch(QueryException $e) {
      Input::flash();
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/users')->with('alert-success', 'User has been updated!');
  }

  public function updateNews(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'category_id' => 'required|integer',
        'title_zh' => 'required',
        'title_en' => 'required',
        'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'event_date' => 'nullable|date'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $catagory_type = DB::table('category')->where('id', $request->input("category_id"))->first()->type;

    if($catagory_type == 4 && $request->input("event_date") == null){
      return redirect()->back()->with('alert-error', 'The event date cannot be empty.');
    }

    $data = array(
      'category_id' => $request->input("category_id"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
      'content_zh' => $request->input("content_zh"),
      'content_en' => $request->input("content_en"),
    );

    if($request->hasfile('cover'))
    {
      if ($request->file('cover')->isValid())
      {

        $file = $request->file('cover');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/blog/', $name);

        $data['cover'] = $name;
      }
    }else{
      if($request->input("remove_img") == 1){
        $data['cover'] = '';
      }
    }

    if($request->input("event_date") != null){
      $data['event_date'] = $request->input("event_date");
    }

    try {
      $updated = DB::table('passage')
            ->where('id', $request->input("id"))
            ->where('approved', 0)
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    if($updated == 0){
      return redirect()->back()->with('alert-error', "This News has been approved. You cannot update this News.");
    }

    return redirect('/admin/news/' . $request->input("issue_id"))->with('alert-success', 'News has been updated!');
  }

  public function updateApprove(Request $request)
  {

    $validator = Validator::make($request->all(), [
        'issue_id' => 'required|integer',
        'category_id' => 'required|integer',
        'title_zh' => 'required',
        'title_en' => 'required',
        'link_zh' => 'nullable|url',
        'link_en' => 'nullable|url',
        'cover' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'event_date' => 'nullable|date'
    ]);

    if ($validator->fails()) {
      return redirect()->back()->with('alert-error', $validator->errors()->first());
    }

    $catagory_type = DB::table('category')->where('id', $request->input("category_id"))->first()->type;

    if($catagory_type == 4 && $request->input("event_date") == null){
      return redirect()->back()->with('alert-error', 'The event date cannot be empty.');
    }

    $data = array(
      'issue_id' => $request->input("issue_id"),
      'category_id' => $request->input("category_id"),
      'title_zh' => $request->input("title_zh"),
      'title_en' => $request->input("title_en"),
      'link_zh' => $request->input("link_zh"),
      'link_en' => $request->input("link_en"),
      'content_zh' => $request->input("content_zh"),
      'content_en' => $request->input("content_en"),
      'passage_publish' => $request->input("passage_publish") == null ? 0 : 1,
      'approved' => 1,
      'approved_by' => Auth::id()
    );

    if($request->hasfile('cover'))
    {
      if ($request->file('cover')->isValid())
      {

        $file = $request->file('cover');
        $name = str_replace('.','-', uniqid(mt_rand(), true))  . "." . $file->extension();
        $file->move(public_path() . '/images/blog/', $name);

        $data['cover'] = $name;
      }
    }else{
      if($request->input("remove_img") == 1){
        $data['cover'] = '';
      }
    }

    if($request->input("event_date") != null){
      $data['event_date'] = $request->input("event_date");
    }

    try {
      DB::table('passage')
            ->where('id', $request->input("id"))
            ->update($data);
    }
    catch(QueryException $e) {
      return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/approve')->with('alert-success', 'Passage has been approved!');
  }

}
