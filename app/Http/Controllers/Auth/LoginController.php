<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $data = [];
        $data['buttons'] = DB::table('buttons')->get()->toArray();
        $data['lang'] = session('lang');

        if ($data['lang'] == null || $data['lang'] == '') {
          $data['lang'] = "en";
        }

        return view('auth.login', $data);
    }

    public function username()
    {
        return 'name';
    }

    protected function loggedOut(Request $request)
    {
      return redirect('/admin/login');
    }

}
