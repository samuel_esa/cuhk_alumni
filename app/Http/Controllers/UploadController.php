<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Controllers\Controller;

class UploadController extends Controller
{

  public function storeImages(Request $request)
  {

    $data = array();

    if($request->hasfile('files'))
    {
      foreach($request->file('files') as $file)
      {
          $name = date('YmdHis') . $file->extension();
          $file->move(public_path() . '/images/blog/', $name);
          $data[] = $name;
      }
    }

    return response()->json($data);
  }

}
