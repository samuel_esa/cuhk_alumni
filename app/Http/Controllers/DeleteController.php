<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;

class DeleteController extends Controller
{

  public function deleteIssue(Request $request)
  {

    try {
      DB::table('issue')
            ->where('id', $request->input("id"))
            ->delete();
    }
    catch(QueryException $e) {
      if($e->getCode() == 23000){
        return redirect()->back()->with('alert-error', "This Issue is assigned to a Passage, you cannot delete this Issue unless you remove all the Passages under this Issue.");
      }else{
        return redirect()->back()->with('alert-error', $e->getMessage());
      }
    }

    return redirect('/admin/issues')->with('alert-success', 'Issue has been deleted!');
  }

  public function deleteCategory(Request $request)
  {

    try {
      $deleted = DB::table('category')
          ->where('id', $request->input("id"))
            ->where('type', '!=', '3')
          ->delete();
    }
    catch(QueryException $e) {
      if($e->getCode() == 23000){
        return redirect()->back()->with('alert-error', "This Category is assigned to a Passage, you cannot delete this Issue unless you remove all the Passages under this Category.");
      }else{
        return redirect()->back()->with('alert-error', $e->getMessage());
      }
    }

    if($deleted == 0){
      return redirect()->back()->with('alert-error', "This is a special category. You cannot delete this category.");
    }

    return redirect('/admin/category')->with('alert-success', 'Category has been deleted!');
  }

  public function deletePassage(Request $request)
  {
    $issue_id = DB::table('passage')
          ->where('id', $request->input("id"))->first()->issue_id;

    try {
      DB::table('passage')
            ->where('id', $request->input("id"))
            ->delete();
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/issues/' . $issue_id)->with('alert-success', 'Passage has been deleted!');
  }

  public function deleteBanner(Request $request)
  {

    try {
      DB::table('banner')
            ->where('id', $request->input("id"))
            ->delete();
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/banner')->with('alert-success', 'Banner has been deleted!');
  }

  public function deleteUsers(Request $request)
  {

    try {
      $deleted = DB::table('users')
            ->where('id', $request->input("id"))
            ->where('group', '!=', 10)
            ->delete();
    }
    catch(QueryException $e) {
      if($e->getCode() == 23000){
        return redirect()->back()->with('alert-error', "Some Passages are written by this User, you cannot delete this User unless you remove all the Passages under this User.");
      }else{
        return redirect()->back()->with('alert-error', $e->getMessage());
      }
    }

    if($deleted == 0){
      return redirect()->back()->with('alert-error', "This is the Admin User. You cannot delete this user.");
    }

    return redirect('/admin/users')->with('alert-success', 'User has been deleted!');
  }

  public function deleteNews(Request $request)
  {

    try {
      $deleted = DB::table('passage')
            ->where('id', $request->input("id"))
            ->where('publish_by', Auth::id())
            ->where('approved', 0)
            ->delete();
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    if($deleted == 0){
      return redirect()->back()->with('alert-error', "You cannot delete this News.");
    }

    return redirect('/admin/news/')->with('alert-success', 'Passage has been deleted!');
  }

  public function deleteApprove(Request $request)
  {
    $issue_id = DB::table('passage')
          ->where('id', $request->input("id"))->first()->issue_id;

    try {
      DB::table('passage')
            ->where('id', $request->input("id"))
            ->delete();
    }
    catch(QueryException $e) {
        return redirect()->back()->with('alert-error', $e->getMessage());
    }

    return redirect('/admin/approve/' . $issue_id)->with('alert-success', 'Passage has been deleted!');
  }

}
