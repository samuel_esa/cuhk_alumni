<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ShouldBeInGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $group, $type = "under")
    {
      if (Auth::check()) {
        if($type == "under" && Auth::user()->group >= $group){
          return $next($request);
        }else if($type == "only" && Auth::user()->group == $group){
          return $next($request);
        }
        return redirect('/admin/home')->with('alert-error', 'You have no permission to enter the page!');
      }

      return redirect('/admin/home')->with('alert-error', 'You have no permission to enter the page!');
    }
}
