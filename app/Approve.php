<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class Approve
{

  public static function getApprove()
  {
    $passages = DB::table('passage')
    ->where('passage.approved', 0)
    ->count();
    
    return $passages;
  }
}
