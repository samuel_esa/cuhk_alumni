﻿var allow_type = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif', 'image/svg'];
var col_no = null;
$(document).ready(function() {

  $('#summernote_zh, #summernote_en').summernote({
      disableDragAndDrop:true,
      height: 400,
      popover: {
        image: [
          ['custom', ['imageAttributes']],
          ['resize', ['resizeFull', 'resizeHalf', 'resizeQuarter']],
          ['float', ['floatLeft', 'floatRight', 'floatNone']],
          ['remove', ['removeMedia']]
        ],
      },
      toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'italic', 'underline', 'clear']],
        // ['font', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
        //['fontname', ['fontname']],
        //['fontsize', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video', 'hr']],
        ['view', ['fullscreen'/*, 'codeview' */]],   // remove codeview button
        ['help', ['help']]
      ],
      lang: 'en-US', // Change to your chosen language
      imageAttributes:{
        icon:'<i class="note-icon-pencil"/>',
        removeEmpty:true, // true = remove attributes | false = leave empty if present
        disableUpload: true // true = don't display Upload Options | Display Upload Options
      },
      callbacks: {
        onPaste: function (e) {
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
          e.preventDefault();
          document.execCommand('insertText', false, bufferText);
        },
        onImageUpload: function (files) {
          sendFiles(files, this);
        }
      },
      onCreateLink: function (url) {
        var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        var phone = /^\+?[\d()-,.]+/
        var schemed = /^[a-z]+:/i
        url = url.trim();
        if (email.test(url)) {
            url = 'mailto:' + url;
        } else if (phone.test(url)) {
                    url = 'tel:' + url.replace(/[ ().\-]/g,'');
        } else if (!schemed.test(url)) {
            url = 'http://' + url;
        }
        return url;
      }
  });

  $('#issue-table').on('click-row.bs.table', function (row, element, field) {
    window.location.href = '/admin/issues/' + element.id;
  });

  $('#category-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "category"){
      window.location.href = '/admin/category/' + element.id;
    }
  });

  $('.passage-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "passage" && field != "is_cover"){
      window.location.href = '/admin/passage/' + element.id;
    }
  });

  $('#banner-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "operate"){
      window.location.href = '/admin/banner/' + element.id;
    }
  });

  $('#buttons-table-1, #buttons-table-2, #buttons-table-3').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "operate"){
      window.location.href = '/admin/button/' + element.id;
    }
  });

  $('#users-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "operate"){
      window.location.href = '/admin/users/' + element.id;
    }
  });

  $('#news-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "operate"){
      window.location.href = '/admin/news/' + element.id;
    }
  });

  $('#approve-table').on('click-cell.bs.table', function (row, field, value, element) {
    if (field != "operate"){
      window.location.href = '/admin/approve/' + element.id;
    }
  });

  $('.datepicker').datepicker({
    format: 'yyyy-mm',
    viewMode: "months",
    minViewMode: "months"
  });

  $('.datepicker-date').datepicker({
    format: 'yyyy-mm-dd',
  });

  $("#category").change(function () {
    if($(this).find(':selected').data('type') == 4){
      $("#event").addClass("d-block");
    }else{
      $("#event").removeClass("d-block");
    }
  });

  $("#type").change(function () {
    if($(this).val() == 1){
      $("#col").addClass("d-block");
      if(col_no != null){
        $("input[name='col_no']").val(col_no);
      }
    }else{
      $("#col").removeClass("d-block");
      col_no = $("input[name='col_no']").val();
      $("input[name='col_no']").val('');
    }
  });

 $("input[type='file']").change(function () {
    if (this.files && this.files[0]) {

        if(!allow_type.includes(this.files[0].type)){
          $("input[type='file']").val('');
          alert("File type incorrect! Please select an image file.");
          return;
        }

        var input = this;
        var reader = new FileReader();

        reader.onload = function (e) {
          if($(input).attr('name') == "ads_pc_zh"){

            $(".ads_pc_zh").children('.upload-cover').css("background-image", "url(" + e.target.result + ")");
            $(".ads_pc_zh-files").addClass("d-none");
            $(".ads_pc_zh").removeClass("d-none");

          }else if($(input).attr('name') == "ads_mobile_zh"){

            $(".ads_mobile_zh").children('.upload-cover').css("background-image", "url(" + e.target.result + ")");
            $(".ads_mobile_zh-files").addClass("d-none");
            $(".ads_mobile_zh").removeClass("d-none");

          }else if($(input).attr('name') == "ads_pc_en"){

            $(".ads_pc_en").children('.upload-cover').css("background-image", "url(" + e.target.result + ")");
            $(".ads_pc_en-files").addClass("d-none");
            $(".ads_pc_en").removeClass("d-none");

          }else if($(input).attr('name') == "ads_mobile_en"){

            $(".ads_mobile_en").children('.upload-cover').css("background-image", "url(" + e.target.result + ")");
            $(".ads_mobile_en-files").addClass("d-none");
            $(".ads_mobile_en").removeClass("d-none");

          }else if($(input).attr('name') == "icon_zh"){

            $(".zh-image").children('.upload-icon').css("background-image", "url(" + e.target.result + ")");
            $(".zh-files").addClass("d-none");
            $(".zh-image").removeClass("d-none");

          }else if($(input).attr('name') == "icon_en"){

            $(".en-image").children('.upload-icon').css("background-image", "url(" + e.target.result + ")");
            $(".en-files").addClass("d-none");
            $(".en-image").removeClass("d-none");

          }else{
            $(".upload-cover").css("background-image", "url(" + e.target.result + ")");
            $(".files").addClass("d-none");
            $(".current-files").removeClass("d-none");
          }

          //$(input).css("outline", "none");
        }

        reader.readAsDataURL(this.files[0]);

        if($(this).attr('name') == "ads_pc_zh"){
          $(".remove-img-btn[data-banner='ads_pc_zh']").removeClass("d-none");
          $(".download-img-btn[data-banner='ads_pc_zh']").addClass("d-none");
          $("input[name='remove_pc_zh']").val(0);
        }else if($(this).attr('name') == "ads_mobile_zh"){
          $(".remove-img-btn[data-banner='ads_mobile_zh']").removeClass("d-none");
          $(".download-img-btn[data-banner='ads_mobile_zh']").addClass("d-none");
          $("input[name='remove_mobile_zh']").val(0);
        }else if($(this).attr('name') == "ads_pc_en"){
          $(".remove-img-btn[data-banner='ads_pc_en']").removeClass("d-none");
          $(".download-img-btn[data-banner='ads_pc_en']").addClass("d-none");
          $("input[name='remove_pc_en']").val(0);
        }else if($(this).attr('name') == "ads_mobile_en"){
          $(".remove-img-btn[data-banner='ads_mobile_en']").removeClass("d-none");
          $(".download-img-btn[data-banner='ads_mobile_en']").addClass("d-none");
          $("input[name='remove_mobile_en']").val(0);
        }else if($(this).attr('name') == "icon_zh"){
          $(".remove-img-btn[data-image='zh']").removeClass("d-none");
          $(".download-img-btn[data-image='zh']").addClass("d-none");
          $("input[name='remove_zh']").val(0);
        }else if($(this).attr('name') == "icon_en"){
          $(".remove-img-btn[data-image='en']").removeClass("d-none");
          $(".download-img-btn[data-image='en']").addClass("d-none");
          $("input[name='remove_en']").val(0);
        }else{
          $(".remove-img-btn").removeClass("d-none");
          $(".download-img-btn").addClass("d-none");
          $("input[name='remove_img']").val(0);
        }

    }
  });

  $(".remove-img-btn").on("click", function(){
      if($(this).data("banner") == "ads_pc_zh"){
        $(".ads_pc_zh-files").removeClass("d-none");
        $(".ads_pc_zh").addClass("d-none");
        $("input[name='ads_pc_zh']").val('');
        $("input[name='remove_pc_zh']").val(1);
      }else if($(this).data("banner") == "ads_mobile_zh"){
        $(".ads_mobile_zh-files").removeClass("d-none");
        $(".ads_mobile_zh").addClass("d-none");
        $("input[name='ads_mobile_zh']").val('');
        $("input[name='remove_mobile_zh']").val(1);
      }else if($(this).data("banner") == "ads_pc_en"){
        $(".ads_pc_en-files").removeClass("d-none");
        $(".ads_pc_en").addClass("d-none");
        $("input[name='ads_pc_en']").val('');
        $("input[name='remove_pc_en']").val(1);
      }else if($(this).data("banner") == "ads_mobile_en"){
        $(".ads_mobile_en-files").removeClass("d-none");
        $(".ads_mobile_en").addClass("d-none");
        $("input[name='ads_mobile_en']").val('');
        $("input[name='remove_mobile_en']").val(1);
      }else if($(this).data("image") == "zh"){
        $(".zh-files").removeClass("d-none");
        $(".zh-image").addClass("d-none");
        $("input[name='icon_zh']").val('');
        $("input[name='remove_zh']").val(1);
      }else if($(this).data("image") == "en"){
        $(".en-files").removeClass("d-none");
        $(".en-image").addClass("d-none");
        $("input[name='icon_en']").val('');
        $("input[name='remove_en']").val(1);
      }else{
        //$(".upload-cover").css("background-image", "none");
        //$(".upload-cover").css("outline", "2px dashed #92b0b3");
        $(".files").removeClass("d-none");
        $(".current-files").addClass("d-none");
        $("input[type='file']").val('');
        $("input[name='remove_img']").val(1);
      }

      $(this).addClass("d-none");
  });

  $(".delete").on("submit", function(){
      return confirm("Are you sure you want to delete this?");
  });

  $(document).on("click", ".seq-btn" , function() {
    var input = $(this).parent().parent().children('input');
    updateSequence(input.data('table'), input.data('id'), input.val(), input.data('col'));
  });

  $(document).on("click", ".cover-btn" , function() {
    $(this).parent().html(
      '<span class="badge badge-light" id="loading">Loading...</span>'
    );

    var current = $('#current_cover').data('id');

    $('#current_cover').parent().html(
      '<button class="btn btn-outline-secondary cover-btn" type="button" data-id="' + current + '"><i class="fa fa-check"></i></button>'
    );

    var id = $(this).data('id');
    updateCover(id, this);
  });

  $('.daterange input').daterangepicker({
    startDate: moment(),
    minDate: moment(),
    locale: {
      format: 'YYYY-MM-DD'
    },
    autoUpdateInput: false,
  }, function(start, end, label) {
    $('input[name="start_date"]').val(start.format('YYYY-MM-DD'));
    $('input[name="end_date"]').val(end.format('YYYY-MM-DD'));
  });

  if($('input[name="approved"]').val() == 1){
    $('#summernote_zh').summernote('disable');
    $('#summernote_en').summernote('disable');
  }

});


function sendFiles(files, el) {

    var formData = new FormData();

    for (var i = 0; i < files.length; i++) {
        formData.append("files[]", files[i]);
    }

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
        data: formData,
        type: "POST",
        url: '/admin/action/upload/images',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
          console.log(response);
            for (var i = 0; i < response.length; i++) {
                $(el).summernote('editor.insertImage', "/images/blog/" + response[i]);
            }
        }
    });
}

function updateSequence(table, id, sequence, col = "sequence") {
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: 'POST',
    url: '/admin/action/update/sequence',
    data: {
        table: table,
        id: id,
        sequence: sequence,
        col: col
    },
    success: function (data) {
        $('div.flash-message').html(data);
        $("div.flash-message").fadeTo(500, 500).slideUp(500, function(){
          $("div.flash-message").slideUp(500);
        });
    },
    error: function (e) {
      console.log(e.responseJSON.message)
    }
  });
}

function updateCover(id, object) {
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: 'POST',
    url: '/admin/action/update/cover',
    data: {
        id: id,
    },
    success: function (data) {
        $('div.flash-message').html(data);
        $("div.flash-message").fadeTo(500, 500).slideUp(500, function(){
          $("div.flash-message").slideUp(500);
        });

        console.log(object);

        $("#loading").parent().html(
          '<span class="badge badge-warning" id="current_cover" data-id="' + id + '" >Cover</span>'
        );

    },
    error: function (e) {
      console.log(e.responseJSON.message)
    }
  });
}

function sequenceFormatter(value, row, index, table) {
    return [
      '<div class="input-group">' +
        '<input type="number" data-id="' + row.id + '" data-table="' + table + '" class="form-control" placeholder="Order" aria-label="sequence" value="' + row.sequence + '">' +
        '<div class="input-group-append">' +
          '<button class="btn btn-outline-secondary seq-btn" type="button"><i class="fa fa-check"></i></button>' +
        '</div>' +
      '</div>'
    ].join('');
}

function setCoverFormatter(value, row, index, table) {
  if(row.is_cover == 1){
    return [
      '<span class="badge badge-warning" id="current_cover" data-id="' + row.id + '" >Cover</span>'
    ].join('');
  }else{
    return [
        '<button class="btn btn-outline-secondary cover-btn" type="button" data-id="' + row.id + '"><i class="fa fa-check"></i></button>'
    ].join('');
  }

}
