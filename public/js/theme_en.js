﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
];

var allow_type = ['image/jpeg', 'image/png', 'image/jpg', 'image/gif', 'image/svg'];
$(document).ready(function(){

  $('#summernote_zh, #summernote_en').summernote({
      disableDragAndDrop:true,
      height: 400,
      popover: {
        image: [
          ['custom', ['imageAttributes']],
          ['resize', ['resizeFull', 'resizeHalf', 'resizeQuarter']],
          ['float', ['floatLeft', 'floatRight', 'floatNone']],
          ['remove', ['removeMedia']]
        ],
      },
      toolbar: [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'hr']],
        ['view', ['fullscreen']],
        ['help', ['help']]
      ],
      lang: 'en-US', // Change to your chosen language
      imageAttributes:{
        icon:'<i class="note-icon-pencil"/>',
        removeEmpty:true, // true = remove attributes | false = leave empty if present
        disableUpload: true // true = don't display Upload Options | Display Upload Options
      },
      callbacks: {
        onPaste: function (e) {
          var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
          e.preventDefault();
          document.execCommand('insertText', false, bufferText);
        },
        onImageUpload: function (files) {
          sendFiles(files, this);
        }
      },
      onCreateLink: function (url) {
        var email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
        var phone = /^\+?[\d()-,.]+/
        var schemed = /^[a-z]+:/i
        url = url.trim();
        if (email.test(url)) {
            url = 'mailto:' + url;
        } else if (phone.test(url)) {
                    url = 'tel:' + url.replace(/[ ().\-]/g,'');
        } else if (!schemed.test(url)) {
            url = 'http://' + url;
        }
        return url;
      }
  });

  $('.datepicker-date').datepicker({
    format: 'yyyy-mm-dd',
  });

  $("#category").change(function () {
    if($(this).find(':selected').data('type') == 4){
      window.location.href = "http://alumni.cuhk.edu.hk/en/eventcalendar/webform";
      //$("#event").addClass("d-block");
    }else{
      //$("#event").removeClass("d-block");
    }
  });

  $(".current-issue").click(function(e){
    e.preventDefault();
    $(".current-issue i").toggleClass("fa-chevron-down");
    $(".current-issue i").toggleClass("fa-chevron-up");
    $(".issue-list").toggleClass("d-none");
  });

  $("#wechat").on("click", function(e){
    e.preventDefault();
  });

  $("#wechat").popover({
    html: true,
    trigger: "focus",
    placement: "top",
    content: function () {
      return '<img src="/images/web/wechat.jpg" height="200"/>';
    }
  });

  $("input[type='file']").change(function () {
     if (this.files && this.files[0]) {

         if(!allow_type.includes(this.files[0].type)){
           $("input[type='file']").val('');
           alert("File type incorrect! Please select an image file.");
           return;
         }

         var input = this;
         var reader = new FileReader();

         reader.onload = function (e) {
             $(".upload-cover").css("background-image", "url(" + e.target.result + ")");
             $(".files").addClass("d-none");
             $(".current-files").removeClass("d-none");
         }

         reader.readAsDataURL(this.files[0]);
         $(".remove-img-btn").removeClass("d-none");
         $("input[name='remove_img']").val(0);
     }
   });

   $(".remove-img-btn").on("click", function(){
       $(".files").removeClass("d-none");
       $(".current-files").addClass("d-none");
       $("input[type='file']").val('');
       $("input[name='remove_img']").val(1);
       $(this).addClass("d-none");
   });

  $("#search button").click(function(){
    if($("#keywords").val() == ''){
      return;
    }

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: "POST",
      url:"/action/search/en",
      data: {
        keywords: $("#keywords").val()
      },
      dataType: "json",
      success:function(result){
        $("#search-result").html('');
        $("#serach-number").show();
        $("#serach-number").html(result.length + " Passages have been found");

        if(result.length > 0){
          result.forEach(function(val, index){

            var date = new Date(val.date);
            var datestring = monthNames[date.getMonth()] + " " + date.getFullYear();


            var link = '/issue/' + val.date + '/' + val.id;
            if(val.link != null){
              link = val.link;
            }

            var passage = val.search_en;

            var content = '';

            if(index > 0){
              content += '<div class="seperator"></div>';
            }

            content += "  <div class='passage-info d-flex align-items-center'>";
            content += "    <div><a target='_blank' href='/en/issue/" + val.date + "'>Issue No." + val.number + "</a></div>";
            content += "    <div class='seperator'></div>";
            content += "    <div><a target='_blank' href='/en/issue/" + val.date + "'>" + datestring + "</a></div>";
            content += "    <div class='seperator'></div>";
            content += "    <div>" + val.name_en + "</div>";
            content += "  </div>";

            content += "  <a target='_blank' href='/en" + link + "'>";
            content += "    <div class='passage-title'>" + val.title_en + "</div>";
            content += "    <div class='content'>";
            content += passage;
            content += "    </div>";
            content += "  </a>";

            $("#search-result").append(content);
          });
        }else{
          var content = '<div class="no-result background-image-contain"></div>';
          $("#search-result").append(content);
        }
      }
    });
  });
});

function sendFiles(files, el) {

    var formData = new FormData();

    for (var i = 0; i < files.length; i++) {
        formData.append("files[]", files[i]);
    }

    $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
        data: formData,
        type: "POST",
        url: '/action/upload/images',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
          console.log(response);
            for (var i = 0; i < response.length; i++) {
                $(el).summernote('editor.insertImage', "/images/blog/" + response[i]);
            }
        }
    });
}
