<?php

return [

    'title' => 'CUHK Alumni Matters',

    'no_issue' => 'Cannot found the issue, please type in correct link.',
    'no_passage' => 'Cannot found the passage, please type in correct link.',

    'home' => 'Home',
    'more' => 'More',
    'back' => 'Back',
    'search' => 'Search',

    'giving_back' => 'Giving Back',
    'giving_back_link' => 'http://alumni.cuhk.edu.hk/en/ways-to-give',
    'career_corner' => 'Career Corner',
    'career_corner_link' => 'http://alumni.cuhk.edu.hk/en/careercorner',
    'update_contact' => 'Update Your<br /> Contact',
    'update_contact_link' => 'http://alumni.cuhk.edu.hk/en/update-your-contact',

    'subscribe' => 'Subscribe',
    'subscribe_link' => 'http://cloud.itsc.cuhk.edu.hk/enewsasp/app/Subscribe.aspx/B35062D262CF0D91606D83C6C78D3D31/',
    'unsubscribe' => 'Unsubscribe',
    'unsubscribe_link' => 'http://cloud.itsc.cuhk.edu.hk/enewsasp/app/UnSubscribe.aspx/B35062D262CF0D91606D83C6C78D3D31/',
    'cu_alumni_magazine' => '“CU Alumni” Magazine',
    'cu_alumni_magazine_link' => 'http://alumni.cuhk.edu.hk/en/magazine',
    'cuhk_e_newsletter' => 'CUHK E-Newsletter',
    'cuhk_e_newsletter_link' => 'https://www.iso.cuhk.edu.hk/english/publications/newsletter/previous-issues.aspx?issueid=2511',
    'login_mycuhk' => 'Login MyCUHK',
    'login_mycuhk_link' => 'https://portal.cuhk.edu.hk/psp/epprd/?cmd=login&languageCd=ENG&',
    'aao_website' => 'AAO Website',
    'aao_website_link' => 'http://alumni.cuhk.edu.hk/en',

    'copyright' => 'Copyright © Alumni Affairs Office, CUHK',
    'inquire' => 'Please send enquiries, suggestions and contributions to cu.aao@cuhk.edu.hk',
    'address' => 'Address: Rm 301, 3/F, John Fulton Centre, The Chinese University of Hong Kong, Sha Tin, N.T., Hong Kong',
    'tel' => 'Tel: 3943 7861',
    'fax' => 'Fax: 2603 6226',

    'preview_news' => 'Preview',
    'news_not_approved' => 'Pending Approval',
    'news_approved' => 'Approved',

];
