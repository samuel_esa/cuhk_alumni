<?php

return [

    'title' => '中大校友電子月報',

    'no_issue' => '找不到對應期刊，請重新輸入網址',
    'no_passage' => '找不到對應文章，請重新輸入網址',

    'home' => '主頁',
    'more' => '更多',
    'back' => '返回',
    'search' => '搜尋',

    'giving_back' => '支持學校',
    'giving_back_link' => 'http://alumni.cuhk.edu.hk/zh-Hant/ways-to-give',
    'career_corner' => '工作機會',
    'career_corner_link' => 'http://alumni.cuhk.edu.hk/zh-Hant/careercorner',
    'update_contact' => '更新地址',
    'update_contact_link' => 'http://alumni.cuhk.edu.hk/zh-Hant/update-your-contact',

    'subscribe' => '訂閱',
    'subscribe_link' => 'http://cloud.itsc.cuhk.edu.hk/enewsasp/app/Subscribe.aspx/D62EFC3A294F7E44E46C65AA225206C6/',
    'unsubscribe' => '取消訂閱',
    'unsubscribe_link' => 'http://cloud.itsc.cuhk.edu.hk/enewsasp/app/UnSubscribe.aspx/D62EFC3A294F7E44E46C65AA225206C6/',
    'cu_alumni_magazine' => '《中大校友》雜誌',
    'cu_alumni_magazine_link' => 'http://alumni.cuhk.edu.hk/zh-Hant/magazine',
    'cuhk_e_newsletter' => '中大通訊',
    'cuhk_e_newsletter_link' => 'https://www.iso.cuhk.edu.hk/chinese/publications/newsletter/previous-issues.aspx?issueid=2511',
    'login_mycuhk' => '登入MyCUHK',
    'login_mycuhk_link' => 'https://portal.cuhk.edu.hk/psp/epprd/?cmd=login&languageCd=ZHT',
    'aao_website' => '校友事務處網頁',
    'aao_website_link' => 'http://alumni.cuhk.edu.hk/zh-Hant',

    'copyright' => '版權 © 香港中文大學校友事務處',
    'inquire' => '查詢、建議及投稿請電郵至 cu.aao@cuhk.edu.hk',
    'address' => '地址：香港 新界 沙田 香港中文大學富爾敦樓301室',
    'tel' => '電話：3943 7861',
    'fax' => '傳真：2603 6226',

    'preview_news' => '預覽文章',
    'news_not_approved' => '等待審批中',
    'news_approved' => '已通過審批',

];
