@extends('layouts.master')

@section('title', 'New News')

@section('content')

<div id="success" class="container container-space text-center">
  <h6>Thank you for submitting the news to CUHK Alumni Matters. For enquiries, please contact us at <a href="mailto:cu.aao@cuhk.edu.hk">cu.aao@cuhk.edu.hk</a>.</h6><br />
  <h6>多謝你的投稿，我們將盡快處理閣下遞交的資料。如有任何查詢，歡迎聯絡我們(<a href="mailto:cu.aao@cuhk.edu.hk">cu.aao@cuhk.edu.hk</a>)。</h6><br />
</div>

@endsection
