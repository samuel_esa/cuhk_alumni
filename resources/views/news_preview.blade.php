<div class="container-fluid preview-bar">
  <div class="container">
    <div class="row justify-content-between align-items-center">
      <div class="">@lang('master.preview_news')</div>
      <div class="">
          @if($passage->approved == 0)
            <span class="badge badge-warning">@lang('master.news_not_approved')</span>
          @else
            <span class="badge badge-success">@lang('master.news_approved')</span>
          @endif
      </div>
    </div>
  </div>
</div>

@extends('layouts.master')

@if($lang == "zh-hk")
  @section('title', $passage->title_zh)
@elseif($lang == "en")
  @section('title', $passage->title_en)
@endif

@section('content')

@empty($passage)
  <div class="container container-space text-center">
    @lang('master.no_issue')
  </div>
@endempty

@isset($passage)
  <section id="pagination" class="container">

    <div class="d-flex justify-content-between">

      <div id="issue">

      </div>

      <ul class="d-flex justify-content-end align-items-center">
        <li><a href="/{{ $lang }}">@lang('master.home')</a></li>
        <li class="seperator"></li>
        <kanhanbypass>
          <li><a href="http://enews.alumni.cuhk.edu.hk/zh-hk/preview/news/{{$passage->approve_id}}">繁</a></li>
          <li><a href="http://translate.itsc.cuhk.edu.hk/uniTS/{{$host}}/zh-hk/preview/news/{{$passage->approve_id}}">简</a></li>
          <li><a href="http://enews.alumni.cuhk.edu.hk/en/preview/news/{{$passage->approve_id}}">ENG</a></li>
        </kanhanbypass>
      </ul>

    </div>

  </section>
@endisset

@isset($passage)

  @if($passage->cover != null && $passage->cover != "")

  <section class="passage-cover container" style="background-image: url('/images/blog/{{ $passage->cover}}');">
  </section>

  @endif

  <section class="passage-content container">

    @if($lang == "zh-hk")
      <h2>{{ $passage->title_zh }}</h2>
    @elseif($lang == "en")
      <h2>{{ $passage->title_en }}</h2>
    @endif

    <div class="passage-html">
      @if($lang == "zh-hk")
        {!! clean($passage->content_zh) !!}
      @elseif($lang == "en")
        {!! clean($passage->content_en) !!}
      @endif
    </div>
  </section>

@endisset

@endsection
