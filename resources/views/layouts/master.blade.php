<!DOCTYPE html>
<html lang="{{ $lang }}">
<head>

  <!-- Designed by ESA Communications Limited -->
  
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>@lang('master.title') - @yield('title')</title>

  <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/lib/fontawesome/css/all.min.css" />
  <link rel="stylesheet" href="/lib/daterangepicker/daterangepicker.min.css" />
  <link rel="stylesheet" href="/lib/summernote/summernote-bs4.css" />
  <link rel="stylesheet" href="/css/theme_{{$lang}}.css" />
  <link rel="stylesheet" href="/css/theme_mobile_{{$lang}}.css" />

  <meta property="og:image" content="/images/web/CUHK_Thumbnail.jpg"/>

</head>

<body>

  <header class="container d-flex justify-content-center align-items-center background-image-cover">
      <a id="logo" href="/{{ $lang }}" class="background-image-contain"></a>
  </header>

  @yield('content')

  <section id="menu-2" class="container">
    <div class="row align-items-start">
      <a href="@if($lang == "zh-hk"){{ $buttons[3]->link_zh }}@elseif($lang == "en"){{ $buttons[3]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[3]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[3]->title_zh }}@elseif($lang == "en"){{ $buttons[3]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[4]->link_zh }}@elseif($lang == "en"){{ $buttons[4]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[4]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[4]->title_zh }}@elseif($lang == "en"){{ $buttons[4]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[5]->link_zh }}@elseif($lang == "en"){{ $buttons[5]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[5]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[5]->title_zh }}@elseif($lang == "en"){{ $buttons[5]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[6]->link_zh }}@elseif($lang == "en"){{ $buttons[6]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[6]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[6]->title_zh }}@elseif($lang == "en"){{ $buttons[6]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[7]->link_zh }}@elseif($lang == "en"){{ $buttons[7]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[7]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[7]->title_zh }}@elseif($lang == "en"){{ $buttons[7]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[8]->link_zh }}@elseif($lang == "en"){{ $buttons[8]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[8]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[8]->title_zh }}@elseif($lang == "en"){{ $buttons[8]->title_en }}@endif</div>
      </a>
      <a href="@if($lang == "zh-hk"){{ $buttons[9]->link_zh }}@elseif($lang == "en"){{ $buttons[9]->link_en }}@endif" target="_blank" class="d-flex flex-column justify-content-center align-items-center">
        <div class="icon background-image-contain" style="background-image: url('/images/icon/{{ $buttons[9]->icon_zh }}');"></div>
        <div class="text-center">@if($lang == "zh-hk"){{ $buttons[9]->title_zh }}@elseif($lang == "en"){{ $buttons[9]->title_en }}@endif</div>
      </a>
    </div>
  </section>

  <section id="bolder" class="container"></section>

  <footer class="container">

      <div class="social flex-center">
        <a href="{{ $buttons[10]->link_zh }}" target="_blank" class="flex-center"><img src="/images/icon/{{ $buttons[10]->icon_zh }}"/></a>
        <a href="{{ $buttons[11]->link_zh }}" target="_blank" class="flex-center"><img src="/images/icon/{{ $buttons[11]->icon_zh }}"/></a>
        <a href="{{ $buttons[12]->link_zh }}" target="_blank" class="flex-center" id="wechat"><img src="/images/icon/{{ $buttons[12]->icon_zh }}"/></a>
        <a href="{{ $buttons[13]->link_zh }}" target="_blank" class="flex-center"><img src="/images/icon/{{ $buttons[13]->icon_zh }}"/></a>
        <a href="{{ $buttons[14]->link_zh }}" target="_blank" class="flex-center"><img src="/images/icon/{{ $buttons[14]->icon_zh }}"/></a>
      </div>

      <div class="info">
        @lang('master.copyright')<br />
        @lang('master.inquire')<br />
        @lang('master.address')<br />
        @lang('master.tel') @lang('master.fax')
      </div>

  </footer>

  <script src="/lib/jquery/dist/jquery.min.js"></script>
  <script src="/lib/popper/popper.min.js"></script>
  <script src="/lib/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="/lib/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="/lib/summernote/summernote-bs4.min.js"></script>
  <script src="/lib/summernote/plugin/image-attributes-master/summernote-image-attributes.js"></script>
  <script src="/js/theme_{{$lang}}.js"></script>

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159221011-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-159221011-1');
  </script>

</body>
</html>

<!-- Developed by Samuel Tsui -->
