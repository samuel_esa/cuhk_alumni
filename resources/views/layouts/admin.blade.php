<?php use \App\Approve; ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>CUHK Alumni Backstage | @yield('title')</title>

  <link rel="stylesheet" href="/lib/bootstrap/dist/css/bootstrap.min.css" />
  <link rel="stylesheet" href="/lib/fontawesome/css/all.min.css" />
  <link rel="stylesheet" href="/lib/bootstrap-table/bootstrap-table.min.css" />
  <link rel="stylesheet" href="/lib/bootstrap-datepicker/bootstrap-datepicker.min.css" />
  <link rel="stylesheet" href="/lib/summernote/summernote-bs4.css" />
  <link rel="stylesheet" href="/lib/daterangepicker/daterangepicker.min.css" />
  <link rel="stylesheet" href="{{ asset('/css/admin.css') }}" />

  <meta property="og:image" content="/images/web/CUHK_Thumbnail.jpg"/>

</head>

<body>

  <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
      <div class="container">
          <a class="navbar-brand" href="{{ url('/admin/home') }}">
              CUHK Alumni Backstage
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
              <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <!-- Left Side Of Navbar -->
              <ul class="navbar-nav mr-auto">
                @if (Auth::user()->group >= 2)
                  @if (strpos(Request::path(), 'admin/issues') !== false)
                    <li class="nav-item active">
                  @else
                    <li class="nav-item">
                  @endif
                        <a class="nav-link" href="/admin/issues">Issue</a>
                    </li>
                    @if (strpos(Request::path(), 'admin/category') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/category">Category</a>
                    </li>
                    @if (strpos(Request::path(), 'admin/passage') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/passage">Passage</a>
                    </li>

                    @if (strpos(Request::path(), 'admin/approve') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/approve">Approve @if(Approve::getApprove() > 0) <span class="badge badge-primary">{{Approve::getApprove()}}</span> @endif</a>
                    </li>

                    @if (strpos(Request::path(), 'admin/banner') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/banner">Banner</a>
                    </li>

                    @if (strpos(Request::path(), 'admin/button') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/button">Button</a>
                    </li>
                  @endif

                  @if (Auth::user()->group >= 10)
                    @if (strpos(Request::path(), 'admin/users') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/users">Users</a>
                    </li>
                  @endif

                  @if (Auth::user()->group == 1)
                    @if (strpos(Request::path(), 'admin/news') !== false)
                      <li class="nav-item active">
                    @else
                      <li class="nav-item">
                    @endif
                        <a class="nav-link" href="/admin/news">News</a>
                    </li>
                  @endif
              </ul>

              <!-- Right Side Of Navbar -->
              <ul class="navbar-nav ml-auto">
                  <!-- Authentication Links -->
                  @guest
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                      @if (Route::has('register'))
                          <li class="nav-item">
                              <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                          </li>
                      @endif
                  @else
                      <li class="nav-item dropdown">
                          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                              {{ Auth::user()->name }} <span class="caret"></span>
                          </a>

                          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                              <a class="dropdown-item" href="{{ route('logout') }}"
                                 onclick="event.preventDefault();
                                               document.getElementById('logout-form').submit();">
                                  {{ __('Logout') }}
                              </a>

                              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  @csrf
                              </form>
                          </div>
                      </li>
                  @endguest
              </ul>
          </div>
      </div>
  </nav>

  <div class="container container-space">
    @if (session('alert-success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('alert-success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
    @endif

    @if (session('alert-error'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('alert-error') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
    @endif
  </div>

  <div class="container container-space">
      @yield('content')
  </div>

  <script src="/lib/jquery/dist/jquery.min.js"></script>
  <script src="/lib/popper/popper.min.js"></script>
  <script src="/lib/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="/lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="/lib/bootstrap-table/bootstrap-table.min.js"></script>
  <script src="/lib/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
  <script src="/lib/summernote/summernote-bs4.min.js"></script>
  <script src="/lib/summernote/plugin/image-attributes-master/summernote-image-attributes.js"></script>
  <script src="/lib/moment/moment.min.js"></script>
  <script src="/lib/daterangepicker/daterangepicker.min.js"></script>
  <script src="/js/admin.js"></script>

</body>
</html>
