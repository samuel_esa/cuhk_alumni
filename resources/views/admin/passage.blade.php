@extends('layouts.admin')

@section('title', 'Passage')

@section('content')
<div id="toolbar">
  <a class="btn btn-success" href="/admin/passage/new" role="button"><i class="fas fa-plus"></i> Add New Passage</a>
</div>

<table class="passage-table" data-toggle="table" data-url="/admin/action/select/passage" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
    <thead>
      <tr>
        <th data-field="number" data-sortable="true">number</th>
        <th data-field="name_zh" data-sortable="true">Category</th>
        <th data-field="title_zh" data-sortable="true">Title</th>
        <th data-field="create_date" data-sortable="true">Create On</th>
        <th data-field="update_date" data-sortable="true">Last Update</th>
        <th data-field="passage_publish" data-sortable="true">Is Published</th>
        <th data-field="name" data-sortable="true">Author</th>
      </tr>
    </thead>
  </table>
@endsection
