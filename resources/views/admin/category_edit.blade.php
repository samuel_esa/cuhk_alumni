@extends('layouts.admin')

@section('title', 'Edit Category')

@section('content')

@if($category->deletable == 1)
<div class="row">
  <div class="col-12 text-right">
      <form action="/admin/action/delete/category" method="post" class="delete">
        <input type="hidden" name="id" value="{{ $category->id }}" required>
        @csrf
        <button type="submit" class="btn btn-danger">Remove</button>
      </form>
    </div>
  </div>
@endif

<h3>Edit Category</h3>
<br />

<form action="/admin/action/update/category" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="category-name">Chinese Category Name</label>
    <input type="text" class="form-control" id="category-name" name="name_zh" placeholder="Category" autocomplete="off" value="{{ $category->name_zh }}" required>
  </div>

  <div class="form-group">
    <label for="category-name">English Category Name</label>
    <input type="text" class="form-control" id="category-name" name="name_en" placeholder="Category" autocomplete="off" value="{{ $category->name_en }}" required>
  </div>

  <div class="form-group files @if($category->icon != "") d-none @endif">
    <label>Upload Icon Image </label>
    <input type="file" class="form-control" name="icon">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center current-files @if($category->icon == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($category->icon != "") style="background-image: url('/images/category/{{ $category->icon }}');" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn">Remove Image</button>
    <a href="/images/category/{{ $category->icon }}" class="btn btn-success download-img-btn" download>Download Image</a>
  </div>

  @if($category->deletable == 1)
    <div class="form-group">
      <label for="type">Display Type</label>
      <select class="custom-select" id="type" name="type" required>
        <option value="">Select Type</option>
        @foreach($category_types as $category_type)
           <option value="{{ $category_type->id }}" @if($category_type->id == $category->type) selected @endif>{{ $category_type->name }}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group @if($category->type == 1) d-block @endif" id="col">
      <label for="col_no">Column Number (1 - 4)</label>
      <input type="number" class="form-control" id="col_no" name="col_no" placeholder="Number of passage in one row" autocomplete="off" value="{{ $category->col_no }}">
    </div>
  @endif

  <input type="hidden" name="remove_img" value="0" required>
  <input type="hidden" name="id" value="{{ $category->id }}" required>
  @csrf
  <button type="submit" class="btn btn-info">Update</button>
</form>

@endsection
