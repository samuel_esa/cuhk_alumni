@extends('layouts.admin')

@section('title', 'Edit Banner')

@section('content')

<div class="row">
  <div class="col-12 text-right">
    <form action="/admin/action/delete/banner" method="post" class="delete">
      <input type="hidden" name="id" value="{{ $banner->id }}" required>
      @csrf
      <button type="submit" class="btn btn-danger">Remove</button>
    </form>
  </div>
</div>

<h3>Edit Banner</h3>
<br />

<form action="/admin/action/update/banner" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="banner-title">Ads Name</label>
    <input type="text" class="form-control" name="ads_title" placeholder="Name" autocomplete="off" required  value="{{ $banner->ads_title }}">
  </div>

  <div class="form-group">
    <label for="link">Chinese Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link" autocomplete="off" value="{{ $banner->link_zh }}">
    <small class="text-info">With http:// or https://</small>
  </div>

  <div class="form-group">
    <label for="link">English Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link" autocomplete="off" value="{{ $banner->link_en }}">
    <small class="text-info">With http:// or https://</small>
  </div>

  <div class="form-group ads_pc_zh-files files @if($banner->ads_pc_zh != "") d-none @endif">
    <label>Upload Chinese Ads Image - PC</label>
    <input type="file" class="form-control" name="ads_pc_zh">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_pc_zh @if($banner->ads_pc_zh == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($banner->ads_pc_zh != "") style="background-image: url('/images/banner/{{ $banner->ads_pc_zh }}'); outline: none" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_pc_zh">Remove Image</button>
    <a href="/images/banner/{{ $banner->ads_pc_zh }}" class="btn btn-success download-img-btn"  data-banner="ads_pc_zh" download>Download Image</a>
  </div>

  <div class="form-group ads_mobile_zh-files files @if($banner->ads_mobile_zh != "") d-none @endif">
    <label>Upload Chinese Ads Image - Mobile</label>
    <input type="file" class="form-control" name="ads_mobile_zh">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_mobile_zh @if($banner->ads_mobile_zh == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($banner->ads_mobile_zh != "") style="background-image: url('/images/banner/{{ $banner->ads_mobile_zh }}'); outline: none" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_mobile_zh">Remove Image</button>
    <a href="/images/banner/{{ $banner->ads_mobile_zh }}" class="btn btn-success download-img-btn"  data-banner="ads_mobile_zh" download>Download Image</a>
  </div>


  <div class="form-group ads_pc_en-files files @if($banner->ads_pc_en != "") d-none @endif">
    <label>Upload English Ads Image - PC</label>
    <input type="file" class="form-control" name="ads_pc_en">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_pc_en @if($banner->ads_pc_en == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($banner->ads_pc_en != "") style="background-image: url('/images/banner/{{ $banner->ads_pc_en }}'); outline: none" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_pc_en">Remove Image</button>
    <a href="/images/banner/{{ $banner->ads_pc_en }}" class="btn btn-success download-img-btn"  data-banner="ads_pc_en" download>Download Image</a>
  </div>

  <div class="form-group ads_mobile_en-files files @if($banner->ads_mobile_en != "") d-none @endif">
    <label>Upload English Ads Image - Mobile</label>
    <input type="file" class="form-control" name="ads_mobile_en">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_mobile_en @if($banner->ads_mobile_en == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($banner->ads_mobile_en != "") style="background-image: url('/images/banner/{{ $banner->ads_mobile_en }}'); outline: none" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_mobile_en">Remove Image</button>
    <a href="/images/banner/{{ $banner->ads_mobile_en }}" class="btn btn-success download-img-btn"  data-banner="ads_mobile_en" download>Download Image</a>
  </div>

  <div class="form-group">
    <label>Ads Date Range</label>
    <div class="input-group daterange">
      <input type="text" class="form-control" name="start_date" placeholder="Start Date" readonly required  value="{{ $banner->start_date }}">
      <div class="input-group-prepend input-group-append">
        <span class="input-group-text">to</span>
      </div>
      <input type="text" class="form-control" name="end_date" placeholder="End Date" readonly required  value="{{ $banner->end_date }}">
    </div>
  </div>

  <div class="form-group">
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="publish" name="publish" @if($banner->publish == 1) checked @endif>
      <label class="custom-control-label" for="publish">Publish</label>
    </div>
  </div>

  <input type="hidden" name="remove_pc_zh" value="0"/>
  <input type="hidden" name="remove_mobile_zh" value="0"/>
  <input type="hidden" name="remove_pc_en" value="0"/>
  <input type="hidden" name="remove_mobile_en" value="0"/>

  <input type="hidden" name="id" value="{{ $banner->id }}"/>

  @csrf
  <button type="submit" class="btn btn-info">Update</button>
</form>


@endsection
