@extends('layouts.admin')

@section('title', 'Update User Information')

@section('content')

<div class="flash-message"></div>

<h3>Edit User</h3>
<br />
<form action="/admin/action/update/users" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col">
      <label for="users-name">Name</label>
      <input type="text" class="form-control" id="users-name" name="name" placeholder="Insert Name" autocomplete="off" required value="{{ $user->name }}">
    </div>

    <div class="form-group col">
      <label for="users-email">Email</label>
      <input type="email" class="form-control" id="users-email" name="email" placeholder="Insert Email" autocomplete="off" required value="{{ $user->email }}">
    </div>
  </div>

  <div class="form-group">
    <label for="users-password">New Password</label>
    <input type="password" class="form-control" id="users-password" name="password" placeholder="Insert New Password" autocomplete="off" value="">
  </div>

  <div class="form-group">
    <label for="users-confirm-password">Confirm Password</label>
    <input type="password" class="form-control" id="users-confirm-password" name="confirm_password" placeholder="Insert Confirm Password" autocomplete="off" value="">
  </div>

  <input type="hidden" name="id" value="{{ $user->id }}" required>
  @csrf
  <button type="submit" class="btn btn-info">Update</button>
</form>

@endsection
