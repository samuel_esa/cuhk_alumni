@extends('layouts.admin')

@section('title', 'Edit Button')

@section('content')

<h3>Edit Button</h3>
<br />

<form action="/admin/action/update/button" method="post" enctype="multipart/form-data">

  @if($button->type == 2)
    <div class="form-group">
      <label for="title_zh">Chinese Title</label>
      <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title" autocomplete="off" value="{{ $button->title_zh }}" required>
    </div>

    <div class="form-group">
      <label for="title_en">English Title</label>
      <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title" autocomplete="off" value="{{ $button->title_en }}" required>
    </div>
  @endif

  <div class="form-group">
    <label for="link">@if($button->type != 3)Chinese @endif Link</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link" autocomplete="off" value="{{ $button->link_zh }}">
    <small class="text-info">With http:// or https://</small>
  </div>

  @if($button->type != 3)
    <div class="form-group">
      <label for="link">English Link</label>
      <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link" autocomplete="off" value="{{ $button->link_en }}">
      <small class="text-info">With http:// or https://</small>
    </div>
  @endif

  <h5>@if($button->type == 1)Chinese @endif Icon</h5>
  <div class="form-group zh-files files @if($button->icon_zh != "") d-none @endif">
    <input type="file" class="form-control" name="icon_zh">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center zh-image @if($button->icon_zh == "") d-none @endif">
    <div class="upload-icon background-image-contain" @if($button->icon_zh != "") style="background-image: url('/images/icon/{{ $button->icon_zh }}');" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-image="zh">Remove Image</button>
    <a href="/images/icon/{{ $button->icon_zh }}" class="btn btn-success download-img-btn"  data-image="zh" download>Download Image</a>
  </div>

  @if($button->type == 1)
    <h5>English Icon</h5>
    <div class="form-group en-files files @if($button->icon_en != "") d-none @endif">
      <input type="file" class="form-control" name="icon_en">
      <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
    </div>

    <div class="form-group text-center en-image @if($button->icon_en == "") d-none @endif">
      <div class="upload-icon background-image-contain" @if($button->icon_en != "") style="background-image: url('/images/icon/{{ $button->icon_en }}');" @endif>
      </div>
      <button type="button" class="btn btn-warning remove-img-btn" data-image="en">Remove Image</button>
      <a href="/images/icon/{{ $button->icon_en }}" class="btn btn-success download-img-btn" data-image="en" download>Download Image</a>
    </div>
  @endif

  <input type="hidden" name="remove_zh" value="0" required>
  @if($button->type == 1)
    <input type="hidden" name="remove_en" value="0" required>
  @endif

  <input type="hidden" name="id" value="{{ $button->id }}" required>
  @csrf
  <button type="submit" class="btn btn-info">Update</button>
</form>
@endsection
