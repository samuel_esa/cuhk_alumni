@extends('layouts.admin')

@section('title', 'New Issue')

@section('content')

<h3>New Issue</h3>
<br />

<form action="/admin/action/insert/issue" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col">
      <label for="issue-number">Number</label>
      <input type="text" class="form-control" id="issue-number" name="number" placeholder="Insert Number" autocomplete="off" required value="{{ old('number') }}">
    </div>
    <div class="form-group col">
      <label for="issue-date">Date</label>
      <input type="text" class="form-control datepicker" id="issue-date" name="date" placeholder="Select Date" autocomplete="off" required value="{{ old('date') }}">
    </div>
  </div>

  <div class="form-group">
    <label for="issue-link">Alumni Link</label>
    <input type="text" class="form-control" id="issue-link" name="alumni_link" placeholder="Insert Link" autocomplete="off" value="{{ old('alumni_link') }}">
    <small class="text-info">The Link format should start with http or https.</small>
  </div>
  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
