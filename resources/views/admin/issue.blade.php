@extends('layouts.admin')

@section('title', 'Issue')

@section('content')
<div id="toolbar">
  <a class="btn btn-success" href="/admin/issues/new" role="button"><i class="fas fa-plus"></i> Add New Issue</a>
</div>

<table id="issue-table" data-toggle="table" data-url="/admin/action/select/issue" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
  <thead>
    <tr>
      <th data-field="number" data-sortable="true">Number</th>
      <th data-field="date" data-sortable="true">Date</th>
      <th data-field="publish" data-sortable="true">Is Published</th>
    </tr>
  </thead>
</table>

@endsection
