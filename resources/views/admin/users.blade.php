@extends('layouts.admin')

@section('title', 'Users')

@section('content')
<div id="toolbar">
  <a class="btn btn-success" href="/admin/users/new" role="button"><i class="fas fa-plus"></i> Add New User</a>
</div>

<table id="users-table" data-toggle="table" data-url="/admin/action/select/users" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
  <thead>
    <tr>
      <th data-field="name" data-sortable="true">Name</th>
      <th data-field="email" data-sortable="true">Email</th>
      <th data-field="group" data-sortable="true">Group</th>
      <th data-field="created_at" data-sortable="true">Created At</th>
      <th data-field="updated_at" data-sortable="true">Updated At</th>
    </tr>
  </thead>
</table>

@endsection
