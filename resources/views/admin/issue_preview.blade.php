<?php use \App\Approve; ?>

@extends('layouts.master')

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand">
            Preview Issue
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
              @if (Auth::user()->group >= 2)
                @if (strpos(Request::path(), 'admin/issues') !== false)
                  <li class="nav-item active">
                @else
                  <li class="nav-item">
                @endif
                      <a class="nav-link" href="/admin/issues/{{$issue->id}}">Back To Edit Issue</a>
                  </li>
              @endif
            </ul>

            @if($issue->publish == 0)
              <span class="badge badge-warning">Not Published</span>
            @else
              <span class="badge badge-success">Published</span>
            @endif

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

@section('title', date('Y年n月號', strtotime($issue->date)))

@section('content')

@empty($issue)
  <div class="container container-space text-center">
    @lang('master.no_issue')
  </div>
@endempty

@isset($issue)
  <section id="pagination" class="container">

    <div class="d-flex justify-content-between">

      <div id="issue">
          <div class="d-flex current-issue">
            <div class="issue-number">
              @if($lang == "zh-hk")
                第{{ $issue->number }}期
              @elseif($lang == "en")
                Issue No. {{ $issue->number }}
              @endif
            </div>
            <div  class="issue-date">
                <span>
                  @if($lang == "zh-hk")
                    {{ date('Y年n月號', strtotime($issue->date)) }}
                  @elseif($lang == "en")
                    {{ date('M Y', strtotime($issue->date)) }}
                  @endif
                </span>
            </div>
          </div>
      </div>

      <ul class="d-flex justify-content-end align-items-center">
        <li><a href="/{{ $lang }}">@lang('master.home')</a></li>
        <li class="seperator"></li>
        <li><a href="/zh-hk/preview/issue/{{$issue->date}}">繁</a></li>
        <li><a href="/en/preview/issue/{{$issue->date}}">ENG</a></li>
      </ul>

    </div>

  </section>
@endisset

@isset($issue)

  @isset($cover_news)

  <a @if ($lang == "zh-hk" && $cover_news->link_zh != '') href="{{ $cover_news->link_zh }}" target="_blank"
    @elseif($lang == "en" && $cover_news->link_en != '') href="{{ $cover_news->link_en }}" target="_blank"
    @elseif($lang == "zh-hk" && $cover_news->link_zh == '' && $cover_news->link_en != '') href="{{ $cover_news->link_en }}" target="_blank"
    @elseif($lang == "en" && $cover_news->link_en == '' && $cover_news->link_zh != '') href="{{ $cover_news->link_zh }}" target="_blank"
    @else href="/{{ $lang }}/issue/{{ $issue->date }}/{{ $cover_news->id}}"
    @endif
    id="cover-news" class="container d-block">
    <div class="row">
      <div class="cover-image col-12 background-image-cover" style="background-image: url('/images/blog/{{ $cover_news->cover}}');">
      </div>
      <div class="seperator col-12"></div>
      <div class="cover-content col-12">
        <div class="d-flex justify-content-between align-items-start">
          @if($lang == "zh-hk")
            <h2>{{ $cover_news->title_zh }}</h2>
          @elseif($lang == "en")
            <h2>{{ $cover_news->title_en }}</h2>
          @endif
          <div class="go-to-btn background-image-contain"></div>
        </div>
        <div class="details">
          @if($lang == "zh-hk")
            {{ str_limit(strip_tags($cover_news->content_zh), 150, '...') }}
          @elseif($lang == "en")
            {{ str_limit(strip_tags($cover_news->content_en), 150, '...') }}
          @endif
        </div>
      </div>
    </div>
  </a>

  @endisset


  <section id="categories" class="container">
    @foreach($categories as $category)

      @if (count($category->passages) > 0 || ($category->type == 3 && $issue->alumni_link != ''))
        <div class="d-flex justify-content-between align-items-center category-bar">
          <div class="seperator flex-fill"></div>
          <div class="icon background-image-cover" style="background-image: url('/images/category/{{ $category->icon}}');"></div>
          <div class="seperator flex-fill"></div>
        </div>

        <div class="category-name text-center">
          @if($lang == "zh-hk")
            {{ $category->name_zh }}
          @elseif($lang == "en")
            {{ $category->name_en }}
          @endif
        </div>

        <!--大學消息 校友優惠-->
        @if($category->type == 1)
          <div class="row column">
          @foreach($category->passages as $passage)
              <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                @else href="/{{ $lang }}/preview/issue/{{ $issue->date }}/{{ $passage->id}}"
                @endif
                 class="d-block col-12 col-md-@php echo (12 / $category->col_no); @endphp"
                >
                <div class="background-image-cover" style="background-image: url('/images/blog/{{ $passage->cover}}');">
                </div>
                <div class="title">
                  @if($lang == "zh-hk")
                    {{ $passage->title_zh }}
                  @elseif($lang == "en")
                    {{ $passage->title_en }}
                  @endif
                </div>
              </a>
          @endforeach
          </div>

          <div class="row more hide">
            <div class="col-12 d-flex justify-content-end">
              <a href="" class="d-flex align-items-center">@lang('master.more') <div class="go-to-btn background-image-contain"></div></a>
            </div>

          </div>

        <!--進修機會-->
        @elseif($category->type == 2)
        <ul class="list">

          @foreach($category->passages as $passage)
            <li class="d-flex">
              <div class="bullet"></div>
              <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                @else href="/{{ $lang }}/preview/issue/{{ $issue->date }}/{{ $passage->id}}"
                @endif
                >
                @if($lang == "zh-hk")
                  {{ $passage->title_zh }}
                @elseif($lang == "en")
                  {{ $passage->title_en }}
                @endif
              </a>
            </li>
          @endforeach
        </ul>

        <!--榜上友名-->
        @elseif($category->type == 3 && $issue->alumni_link != '')

        <a href="{{$category->alumni->url}}" target="_blank" class="row alumni">
          <div class="col-12 cover background-image-cover" style="background-image: url('{{ $category->alumni->images[0]->url}}');"></div>
          <div class="col-12 content">
            <div class="d-flex justify-content-between align-items-start">
              <h2>{{ $category->alumni->title }}</h2>
              <div class="go-to-btn background-image-contain"></div>
            </div>
            <div class="details">
              {{ str_limit(strip_tags($category->alumni->description), 150, '...') }}
            </div>
          </div>
        </a>

        <!--校友活動-->
        @elseif($category->type == 4)
          <div class="row events">
            @foreach($category->passages as $passage)
            <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
              @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
              @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
              @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
              @else href="/{{ $lang }}/preview/issue/{{ $issue->date }}/{{ $passage->id}}"
              @endif
               class="col-12 col-md-6 d-flex align-items-center">
                <div class="background-image-contain calender">
                  <div class="text-center month">
                    {{ strtoupper(date("M", strtotime($passage->event_date))) }}
                  </div>
                  <div class="text-center day">
                    {{ strtoupper(date("j", strtotime($passage->event_date))) }}
                  </div>
                </div>
                <div class="title">
                  @if($lang == "zh-hk")
                    {{ $passage->title_zh }}
                  @elseif($lang == "en")
                    {{ $passage->title_en }}
                  @endif
                </div>
              </a>
            @endforeach
          </div>

        @endif
      @endif
    @endforeach
  </section>
@endisset

@isset($banner)
  <section id="banner" class="container">
    @if($lang == "zh-hk")
      <div class="row">
        <a href="{{ $banner->link_zh }}" target="_blank" class="background-image-cover ads_pc" style="background-image: url('/images/banner/{{ $banner->ads_pc_zh }}');"></a>
        <a href="{{ $banner->link_zh }}" target="_blank" class="background-image-cover ads_mobile" style="background-image: url('/images/banner/{{ $banner->ads_mobile_zh }}');"></a>
      </div>
    @elseif($lang == "en")
      <a href="{{ $banner->link_en }}" target="_blank" class="background-image-cover ads_pc" style="background-image: url('/images/banner/{{ $banner->ads_pc_en }}');"></a>
      <a href="{{ $banner->link_en }}" target="_blank" class="background-image-cover ads_mobile" style="background-image: url('/images/banner/{{ $banner->ads_mobile_en }}');"></a>
    @endif
  </section>
@endisset

<section id="menu-1" class="container">
  <div class="row justify-content-center">
    <a href="@if($lang == "zh-hk"){{ $buttons[0]->link_zh }}@elseif($lang == "en"){{ $buttons[0]->link_en }}@endif" target="_blank" class="d-block background-image-cover" style="background-image: url('/images/icon/@if($lang == "zh-hk"){{ $buttons[0]->icon_zh }}@elseif($lang == "en"){{ $buttons[0]->icon_en }}@endif');">
    </a>
    <a href="@if($lang == "zh-hk"){{ $buttons[1]->link_zh }}@elseif($lang == "en"){{ $buttons[1]->link_en }}@endif" target="_blank" class="d-block background-image-cover" style="background-image: url('/images/icon/@if($lang == "zh-hk"){{ $buttons[1]->icon_zh }}@elseif($lang == "en"){{ $buttons[1]->icon_en }}@endif');">
    </a>
    <a href="@if($lang == "zh-hk"){{ $buttons[2]->link_zh }}@elseif($lang == "en"){{ $buttons[2]->link_en }}@endif" target="_blank" class="d-block background-image-cover" style="background-image: url('/images/icon/@if($lang == "zh-hk"){{ $buttons[2]->icon_zh }}@elseif($lang == "en"){{ $buttons[2]->icon_en }}@endif');">
    </a>
  </div>
</section>




@endsection
