<!DOCTYPE html>
<html lang="{{$lang}}" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="x-apple-disable-message-reformatting">
  <meta name="format-detection" content="telephone=no,address=no,email=no,date=no,url=no">
  <title></title>

  <style>

  @if($lang == "zh-hk")
    @font-face {
      font-family: "Microsoft Jhenghei";
      src: url("{{$data['protocol']}}://{{$data['domain']}}/fonts/microsoft_jhenghei.ttf");
    }
    @font-face {
      font-family: "Microsoft Jhenghei";
      src: url("{{$data['protocol']}}://{{$data['domain']}}/fonts/microsoft_jhenghei_bold.ttf");
      font-weight: bold;
    }
  @endif

  html,
  body {
    margin: 0 !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
    background-color: #E3E3E3;
  }

  * {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }

  div[style*="margin: 16px 0"] {
    margin: 0 !important;
  }

  table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }

  table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
  }

  a {
    text-decoration: none;
    text-decoration: none !important;
  }

  img {
    -ms-interpolation-mode:bicubic;
  }

  a[x-apple-data-detectors],
  .unstyle-auto-detected-links a,
  .aBn {
    border-bottom: 0 !important;
    cursor: default !important;
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
  }

  .im {
    color: #555555 !important;
  }

  .a6S {
    display: none !important;
    opacity: 0.01 !important;
  }

  img.g-img + div {
    display: none !important;
  }

  @media  only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
      min-width: 320px !important;
    }
  }

  @media  only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
      min-width: 375px !important;
    }
  }

  @media  only screen and (min-device-width: 414px) {
    u ~ div .email-container {
      min-width: 414px !important;
    }
  }

  @if($lang == "zh-hk")
  .mso-font{
    font-size: 14px;
  }
  @else
  .mso-font{
    font-size: 13px;
  }
  @endif

  .stack-column-center{
    width: 50%;
    float: left;
  }

  td .stack-column-center:nth-child(2n+1){
    clear: left
  }

  .menu-table-1, .menu-table-2{
    float: left;
  }

  .menu-table-1{
    width: 57.142857%;
  }

  .menu-table-2{
    width:42.857143%;
  }

</style>

<!--[if gte mso 9]>
  <xml>
  <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
  </xml>

  <style>
    html,body{
      font-family: '微軟正黑體';
    }
  </style>
<![endif]-->

<!--[if !mso]>-->
  <style>
    @if($lang == "zh-hk")
      html,body{
        font-family: 'Microsoft Jhenghei';
      }
    @else
      html,body{
        font-family: Arial;
      }
    @endif
  </style>
<!--[endif]-->

<style>

  @media screen and (max-width: 710px) {

    @if($lang == "zh-hk")
      .mso-font{
        font-size: 12px;
      }
    @else
      .mso-font{
        font-size: 11px;
      }
    @endif

    .email-container {
      width: 100% !important;
      margin: auto !important;
    }

    .stack-column-center {
      display: block !important;
      width: 100% !important;
      max-width: 100% !important;
      direction: ltr !important;
    }

    .stack-column-center {
      text-align: center !important;
    }

    .center-on-narrow {
      text-align: center !important;
      display: block !important;
      margin-left: auto !important;
      margin-right: auto !important;
      float: none !important;
    }

    table.center-on-narrow {
      display: inline-block !important;
    }
  }

  @media screen and (max-width: 600px) {
    .menu-table-1, .menu-table-2{
      float: none;
    }

    .menu-table-1{
      width: 100%;
    }

    .menu-table-2{
      width: 100%;
    }
  }

  @media  screen and (max-width: 350px) {
    @if($lang == "zh-hk")
      .email-container span {
        font-size: 12px !important;
      }
    @else
      .email-container span {
        font-size: 11px !important;
      }
    @endif
  }

  </style>

</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #E3E3E3;">

  <style>

  @if($lang == "zh-hk")
    @font-face {
      font-family: "Microsoft Jhenghei";
      src: url("{{$data['protocol']}}://{{$data['domain']}}/fonts/microsoft_jhenghei.ttf");
    }
    @font-face {
      font-family: "Microsoft Jhenghei";
      src: url("{{$data['protocol']}}://{{$data['domain']}}/fonts/microsoft_jhenghei_bold.ttf");
      font-weight: bold;
    }
  @endif

  html,
  body {
    margin: 0 !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
    background-color: #E3E3E3;
  }

  * {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
  }

  div[style*="margin: 16px 0"] {
    margin: 0 !important;
  }

  table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }

  table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
  }

  a {
    text-decoration: none;
    text-decoration: none !important;
  }

  img {
    -ms-interpolation-mode:bicubic;
  }

  a[x-apple-data-detectors],
  .unstyle-auto-detected-links a,
  .aBn {
    border-bottom: 0 !important;
    cursor: default !important;
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
  }

  .im {
    color: #555555 !important;
  }

  .a6S {
    display: none !important;
    opacity: 0.01 !important;
  }

  img.g-img + div {
    display: none !important;
  }

  @media  only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
      min-width: 320px !important;
    }
  }

  @media  only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
      min-width: 375px !important;
    }
  }

  @media  only screen and (min-device-width: 414px) {
    u ~ div .email-container {
      min-width: 414px !important;
    }
  }

  @if($lang == "zh-hk")
    .mso-font{
      font-size: 14px;
    }
  @else
    .mso-font{
      font-size: 13px;
    }
  @endif

  .stack-column-center{
    width: 50%;
    float: left;
  }

  td .stack-column-center:nth-child(2n+1){
    clear: left
  }

  .menu-table-1, .menu-table-2{
    float: left;
  }

  .menu-table-1{
    width: 57.142857%;
  }

  .menu-table-2{
    width:42.857143%;
  }

</style>

<!--[if gte mso 9]>
  <xml>
  <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
  </xml>

  <style>
    @if($lang == "zh-hk")
      html,body{
        font-family: '微軟正黑體';
      }
    @else
      html,body{
        font-family: Arial;
      }
    @endif
  </style>
<![endif]-->

<!--[if !mso]>-->
  <style>
    @if($lang == "zh-hk")
      html,body{
        font-family: 'Microsoft Jhenghei';
      }
    @else
      html,body{
        font-family: Arial;
      }
    @endif
  </style>
<!--[endif]-->

<style>

  @media screen and (max-width: 710px) {


    @if($lang == "zh-hk")
      .mso-font{
        font-size: 12px;
      }
    @else
      .mso-font{
        font-size: 11px;
      }
    @endif

    .email-container {
      width: 100% !important;
      margin: auto !important;
    }

    .stack-column-center {
      display: block !important;
      width: 100% !important;
      max-width: 100% !important;
      direction: ltr !important;
    }

    .stack-column-center {
      text-align: center !important;
    }

    .center-on-narrow {
      text-align: center !important;
      display: block !important;
      margin-left: auto !important;
      margin-right: auto !important;
      float: none !important;
    }

    table.center-on-narrow {
      display: inline-block !important;
    }
  }

  @media screen and (max-width: 600px) {
    .menu-table-1, .menu-table-2{
      float: none;
    }

    .menu-table-1{
      width: 100%;
    }

    .menu-table-2{
      width: 100%;
    }
  }

  @media  screen and (max-width: 350px) {
    @if($lang == "zh-hk")
      .email-container span {
        font-size: 12px !important;
      }
    @else
      .email-container span {
        font-size: 11px !important;
      }
    @endif
  }

  </style>

  <center style="width: 100%; background-color: #E3E3E3;">

    <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="710" style="margin: auto; background-color: #ffffff;" class="email-container">

      <tr>
        <td valign="middle" style="text-align: center; background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/web/masthead-background@2x.png'); background-repeat: no-repeat; background-size: cover !important; background-position: center !important;">

          <!--[if gte mso 9]>
            <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width:710px;height:110px; background-position: center center !important;">
              <v:fill type="frame" src="{{$data['protocol']}}://{{$data['domain']}}/images/web/masthead-background@2x.png" color="#E3E3E3" />
              <v:textbox inset="auto, 20px, auto, 20px" style="v-text-align: center;">
          <![endif]-->

            <table cellpadding="0" cellspacing="0" border="0" width="100%">
              <tr>
                <td align="center">
                  <a href="{{$data['protocol']}}://{{$data['domain']}}/zh-hk" style="display:block; padding: 20px 0px; text-align: center; text-decoration:none;" align="center">
                    <img src="{{$data['protocol']}}://{{$data['domain']}}/images/web/logo-alumni-matters@2x.png" width="232" height="70" alt="alt_text" border="0" style="height: auto; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555;">
                  </a>
                </td>
              </tr>
            </table>

          <!--[if gte mso 9]>
              </v:textbox>
            </v:rect>
          <![endif]-->

        </td>
      </tr>

      <tr>
        <td style="padding: 5px 25px; background-color: #FFFFFF;">

          <table cellpadding="0" cellspacing="0" border="0" width="100%" style="font-size: @if($lang == "zh-hk") 14px @else 13px @endif; font-weight: bold; ">
            <tr>
              <td align="center" style="padding-right: 8.79px;" width="@if($lang == "zh-hk") 65 @else 89 @endif">
                @if($lang == "zh-hk")
                  第{{ $data['issue']->number }}期
                @elseif($lang == "en")
                  Issue No. {{ $data['issue']->number }}
                @endif
              </td>
              <td align="center" style="background-color: #750F6D" width="1"></td>
              <td align="left" style="padding-left: 8.79px;">
                @if($lang == "zh-hk")
                  {{ date('Y年n月號', strtotime($data['issue']->date)) }}
                @elseif($lang == "en")
                  {{ date('M Y', strtotime($data['issue']->date)) }}
                @endif
              </td>
            </tr>
          </table>

        </td>
      </tr>

      @if($data['cover_news'] != null)

      <tr>
        <td style="background-color: #ffffff;">
        <a @if ($lang == "zh-hk" && $data['cover_news']->link_zh != '') href="{{ $data['cover_news']->link_zh }}"
          @elseif($lang == "en" && $data['cover_news']->link_en != '') href="{{ $data['cover_news']->link_en }}"
          @elseif($lang == "zh-hk" && $data['cover_news']->link_zh == '' && $data['cover_news']->link_en != '') href="{{ $data['cover_news']->link_en }}"
          @elseif($lang == "en" && $data['cover_news']->link_en == '' && $data['cover_news']->link_zh != '') href="{{ $data['cover_news']->link_zh }}"
          @else href="{{$data['protocol']}}://{{$data['domain']}}/{{$lang}}/issue/{{ $data['issue']->date }}/{{ $data['cover_news']->id}}"
          @endif
          style="text-decoration:none;">
            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/blog/{{ $data['cover_news']->cover}}" width="710" height="356.94" alt="alt_text" border="0" style="width: 100%; max-width: 710px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555; margin: auto; display: block;" class="g-img">
          </a>
        </td>
      </tr>

      <tr>
        <td style="height: 2px; background: transparent linear-gradient(90deg, #DDA300 0%, #750F6D 100%) 0% 0% no-repeat padding-box;" width="100%">

          <!--[if gte mso 9]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <td style="height: 2px;">
                  <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style="width: 710px; height:2px;" fillcolor="#750F6D;">
                    <v:fill color2="#DDA300" type="gradient" angle="-90"/>
                  </v:rect>
                </td>
              </tr>
            </table>
          <![endif]-->

        </td>
      </tr>

      <tr>
        <td style="background-color: #ffffff;">
          <a href="{{$data['protocol']}}://{{$data['domain']}}/{{$lang}}/issue/{{ $data['issue']->date }}/{{ $data['cover_news']->id}}" style="text-decoration:none;">
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
              <tr>
                <td style="padding: 15px 25px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;">
                  <h1 style="margin: 0 0 16px; font-size: @if($lang == "zh-hk") 20px @else 19px @endif; line-height: 30px; color: #750F6D; font-weight: bold; ">
                    @if($lang == "zh-hk")
                      {{ $data['cover_news']->title_zh}}
                    @elseif($lang == "en")
                      {{ $data['cover_news']->title_en}}
                    @endif
                  </h1>
                  <p style="margin: 0 0 10px; font-size:@if($lang == "zh-hk") 14px @else 13px @endif; ">
                    @if($lang == "zh-hk")
                      {{str_limit(strip_tags($data['cover_news']->content_zh), 150, '...')}}
                    @elseif($lang == "en")
                      {{str_limit(strip_tags($data['cover_news']->content_en), 150, '...')}}
                    @endif
                  </p>
                </td>
              </tr>
            </table>
          </a>
        </td>
      </tr>

      @endif

      @foreach($data['categories'] as $category)

        @if (count($category->passages) > 0 || ($category->type == 3 && $data['issue']->alumni_link != ''))
          <tr>
            <td style="background-color: #ffffff; padding: 0px 25px; ">
              <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                <tr>
                  <td style="text-align: center; padding-right: 25px;">
                    <!--[if gte mso 9]>
                      <img src="{{$data['protocol']}}://{{$data['domain']}}/images/web/seperator_category.jpg" width="263" height="3" alt="alt_text" border="0" style="width: 100%; max-width: 263px; height: 3;">
                    <![endif]-->
                    <!--[if !mso]>-->

                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                        <td style="height: 3px; background-color: #750F6D; min-height: 3px; line-height:3px; font-size:3px;" height="3">
                        </td>
                      </tr>
                    </table>
                    <!--[endif]-->
                  </td>
                  <td style="text-align: center;" width="73">
                    <img src="{{$data['protocol']}}://{{$data['domain']}}/images/category/{{ $category->icon}}" width="73" height="" alt="alt_text" border="0" style="width: 100%; max-width: 73px; height: auto; background: #ffffff;">
                  </td>
                  <td style="text-align: center; padding-left: 25px;">
                    <!--[if gte mso 9]>
                      <img src="{{$data['protocol']}}://{{$data['domain']}}/images/web/seperator_category.jpg" width="263" height="3" alt="alt_text" border="0" style="width: 100%; max-width: 263px; height: 3;">
                    <![endif]-->
                    <!--[if !mso]>-->

                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                        <td style="height: 3px; background-color: #750F6D; min-height: 3px; line-height:3px; font-size:3px;" height="3">
                        </td>
                      </tr>
                    </table>
                    <!--[endif]-->
                  </td>
                </tr>
                <tr>
                  <td colspan="3" style="padding-top: 10px; padding-bottom: 26px; font-size:@if($lang == "zh-hk") 18px @else 17px @endif; font-weight: bold; color: #750F6D; text-align: center; ">
                    @if($lang == "zh-hk")
                      {{ $category->name_zh}}
                    @elseif($lang == "en")
                      {{ $category->name_en}}
                    @endif
                  </td>
                </tr>
              </table>
            </td>
          </tr>

          <!-- Grid -->
          @if($category->type == 1)

            <tr>
              <td style="padding: 0px 10px; background-color: #ffffff;">
                <!--[if gte mso 9]>
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                <![endif]-->

                @php
                  $i = 0;
                @endphp
                @foreach($category->passages as $passage)

                  @php
                    if($i % 2 == 0){
                      echo "<!--[if gte mso 9]><tr><![endif]-->";
                    }
                  @endphp

                  <!--[if gte mso 9]>
                    <td valign="top">
                  <![endif]-->

                  <div class="stack-column-center">
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                          <td style="padding: 0px 10px; text-align: center">
                            <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @else href="{{$data['protocol']}}://{{$data['domain']}}/{{ $lang }}/issue/{{ $data['issue']->date }}/{{ $passage->id}}"
                              @endif
                              style="text-decoration:none;">

                                <!--[if gte mso 9]>
                                  <v:image style='width: 320px; height: 210px;' src="{{$data['protocol']}}://{{$data['domain']}}/images/blog/{{ $passage->cover}}" />
                                <![endif]-->

                                <!--[if !mso]>-->
                                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/blog/{{ $passage->cover}}" width="320" alt="alt_text" border="0" style="width: 100%; height: 210px; object-fit: cover; object-position: top center;">
                                <!--[endif]-->

                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td style="font-family: sans-serif; font-size: @if($lang == "zh-hk") 16px @else 15px @endif; height:50px; color: #555555; padding: 0 10px; text-align: left; font-weight: bold; overflow: hidden; margin-bottom: 16px;" class="center-on-narrow" valign="top">
                            <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @else href="{{$data['protocol']}}://{{$data['domain']}}/{{ $lang }}/issue/{{ $data['issue']->date }}/{{ $passage->id}}"
                              @endif
                              style="text-decoration:none; color: #555555;">
                                @if($lang == "zh-hk")
                                {{ $passage->title_zh}}
                                @elseif($lang == "en")
                                {{ $passage->title_en}}
                                @endif
                            </a>
                          </td>
                        </tr>
                      </table>
                  </div>

                  <!--[if gte mso 9]>
                    </td>
                  <![endif]-->

                  @php
                    if($i % 2 == 1 || $i == sizeof($category->passages)){
                      echo "<!--[if gte mso 9]></tr><![endif]-->";
                    }

                    $i++;
                  @endphp

                @endforeach

                <!--[if gte mso 9]>
                  </table>
                <![endif]-->

              </td>
            </tr>

          <!-- List -->
          @elseif($category->type == 2)
            <tr>
              <td style="background-color: #ffffff; padding: 0px 25px; ">
                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                  @foreach($category->passages as $passage)
                  <tr>
                    <td style="" width="20">
                      <!--[if gte mso 9]>
                          <img src="{{$data['protocol']}}://{{$data['domain']}}/images/web/dot_list.jpg" width="4" height="4" alt="alt_text" border="0" style="width: 100%; max-width: 4px; height: 4;">
                      <![endif]-->
                      <!--[if !mso]>-->
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="4">
                        <tr>
                          <td style="height: 4px; background-color: #DDA300; font-size: 4px; line-height: 4px;" height="4">
                          </td>
                        </tr>
                      </table>
                      <!--[endif]-->
                    </td>
                    <td style="padding: 3.5px 0px;">
                    <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                      @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                      @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                      @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                      @else href="{{$data['protocol']}}://{{$data['domain']}}/{{ $lang }}/issue/{{ $data['issue']->date }}/{{ $passage->id}}"
                      @endif
                      style="color: #555555; font-size: @if($lang == "zh-hk") 16px @else 15px @endif; font-weight: bold; text-decoration:none; ">
                        @if($lang == "zh-hk")
                          {{ $passage->title_zh}}
                        @elseif($lang == "en")
                          {{ $passage->title_en}}
                        @endif
                      </a>
                    </td>
                  </tr>
                  @endforeach
                </table>
              </td>
            </tr>

          <!-- Alumni -->
          @elseif($category->type == 3 && $data['issue']->alumni_link != '')
            <tr>
              <td style="background-color: #ffffff;">
                <a href="{{$category->alumni->url}}" style="text-decoration:none;">
                  <img src="{{ $category->alumni->images[0]->url}}" width="710" alt="alt_text" border="0" style="width: 100%; max-width: 710px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; line-height: 15px; color: #555555; margin: auto; display: block;" class="g-img">
                </a>
              </td>
            </tr>

            <tr>
              <td style="background-color: #ffffff;">
                <a href="{{$category->alumni->url}}" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="padding: 15px 25px; font-family: sans-serif; font-size: @if($lang == "zh-hk") 15px @else 14px @endif; line-height: 20px; color: #555555;">
                        <h1 style="margin: 0 0 16px; font-size: @if($lang == "zh-hk") 20px @else 19px @endif; line-height: 30px; color: #750F6D; font-weight: bold; ">{{ $category->alumni->title }}</h1>
                        <p style="margin: 0 0 10px; ">
                          {{str_limit(strip_tags($category->alumni->description), 150, '...')}}</p>
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
            </tr>

          <!-- Event -->
          @elseif($category->type == 4)

            <tr>
              <td style="background-color: #ffffff; padding: 0px 25px;">

                <!--[if gte mso 9]>
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                <![endif]-->

                @php
                  $i = 0;
                @endphp
                @foreach($category->passages as $passage)

                  @php
                    if($i % 2 == 0){
                      echo "<!--[if gte mso 9]><tr><![endif]-->";
                    }
                  @endphp

                  <!--[if gte mso 9]>
                    <td>
                  <![endif]-->

                  <div class="stack-column-center">
                      <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="330">
                        <tr>
                          <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/web/calendar@2x.png'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="70" height="70">

                          <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                            @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                            @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                            @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                            @else href="{{$data['protocol']}}://{{$data['domain']}}/{{ $lang }}/issue/{{ $data['issue']->date }}/{{ $passage->id}}"
                            @endif
                            style="text-decoration:none;">

                              <!--[if gte mso 9]>
                                <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 70px; height: 70px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/web/calendar@2x.png" />
                                <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 70px; height:70px;">
                                  <v:fill  opacity="0%" color="#000000”  />
                                  <v:textbox inset="0,0,0,0">
                              <![endif]-->

                              <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" height="100%">
                                <tr>
                                  <td style="text-align:center; font-family: Arial; color: #ffffff; font-size: 18px; font-weight: bold; vertical-align: bottom; padding-bottom: 2px;">
                                    {{ strtoupper(date("M", strtotime($passage->event_date))) }}
                                  </td>
                                </tr>
                                <tr>
                                  <td style="text-align:center; font-family: Arial; color: #750F6D; font-size: 30px; font-weight: bold; vertical-align: top;">
                                    {{ strtoupper(date("j", strtotime($passage->event_date))) }}
                                  </td>
                                </tr>
                              </table>

                              <!--[if gte mso 9]>
                                  </v:textbox>
                                </v:rect>
                              <![endif]-->

                            </a>

                          </td>
                          <td style="font-family: sans-serif; font-size: @if($lang == "zh-hk") 16px @else 15px @endif; line-height: 21px; color: #555555; padding: 0px 15px; text-align: left; font-weight: bold; ">
                            <a @if ($lang == "zh-hk" && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "zh-hk" && $passage->link_zh == '' && $passage->link_en != '') href="{{ $passage->link_en }}" target="_blank"
                              @elseif($lang == "en" && $passage->link_en == '' && $passage->link_zh != '') href="{{ $passage->link_zh }}" target="_blank"
                              @else href="{{$data['protocol']}}://{{$data['domain']}}/{{ $lang }}/issue/{{ $data['issue']->date }}/{{ $passage->id}}"
                              @endif
                              style="text-decoration:none; color: #555555;">

                                @if($lang == "zh-hk")
                                  {{ $passage->title_zh}}
                                @elseif($lang == "en")
                                  {{ $passage->title_en}}
                                @endif
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td height="27"></td>
                        </tr>
                      </table>
                    </a>
                  </div>

                  <!--[if gte mso 9]>
                    </td>
                  <![endif]-->

                  @php
                    if($i % 2 == 1 || $i == sizeof($category->passages)){
                      echo "<!--[if gte mso 9]></tr><![endif]-->";
                    }

                    $i++;
                  @endphp

                @endforeach

                <!--[if gte mso 9]>
                  </table>
                <![endif]-->

              </td>
            </tr>

          @endif
        @endif
      @endforeach

      <tr>
        <td style="background-color: #ffffff; padding: 27.86px 25px 0px 25px;">
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="max-width: 410px; " align="center">
            <tr>
              <td>
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" height="82" align="center">
                    <tr>
                      <td width="82" height="82" align="center">

                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][0]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][0]->link_en }}@endif" style="text-decoration:none;">
                          <!--[if !mso]>-->
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][0]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][0]->icon_en }}@endif" width="100%" height="" alt="alt_text" border="0" style="width: auto; height: 82px; max-width: 82px;">
                          <!--[endif]-->
                          <!--[if gte mso 9]>
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][0]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][0]->icon_en }}@endif" width="82" height="82" alt="alt_text" border="0" style="width: 82px; height: 82px; max-width: 82px;">
                          <![endif]-->
                        </a>

                      </td>
                    </tr>
                  </table>
              </td>
              <td width="15"></td>
              <td>
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" height="82" align="center">
                    <tr>
                      <td width="82" height="82" align="center">

                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][1]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][1]->link_en }}@endif">
                          <!--[if !mso]>-->
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][1]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][1]->icon_en }}@endif" width="100%" height="" alt="alt_text" border="0" style="width: auto; height: 82px; max-width: 82px;">
                          <!--[endif]-->
                          <!--[if gte mso 9]>
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][1]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][1]->icon_en }}@endif" width="82" height="82" alt="alt_text" border="0" style="width: 82px; height: 82px; max-width: 82px;">
                          <![endif]-->
                        </a>
                      </td>
                    </tr>
                  </table>
              </td>
              <td width="15"></td>
              <td>
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" height="82" align="center">
                    <tr>
                      <td width="82" height="82" align="center">

                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][2]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][2]->link_en }}@endif">
                          <!--[if !mso]>-->
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][2]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][2]->icon_en }}@endif" width="100%" height="" alt="alt_text" border="0" style="width: auto; height: 82px; max-width: 82px;">
                          <!--[endif]-->
                          <!--[if gte mso 9]>
                            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/@if($lang == "zh-hk"){{ $data['buttons'][2]->icon_zh }}@elseif($lang == "en"){{ $data['buttons'][2]->icon_en }}@endif" width="82" height="82" alt="alt_text" border="0" style="width: 82px; height: 82px; max-width: 82px;">
                          <![endif]-->
                        </a>
                      </td>
                    </tr>
                  </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td style="background-color: #ffffff; padding: 37px 25px 17px 25px;" >

          <!--[if !mso]>-->

          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center" class="menu-table-1">
            <tr>
              <td valign="top" width="25%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][3]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][3]->link_en }}@endif">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][3]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][3]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][3]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][3]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
              <td valign="top" width="25%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][4]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][4]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][4]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][4]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][4]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][4]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
              <td valign="top" width="25%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][5]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][5]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][5]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][5]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][5]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][5]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
              <td valign="top" width="25%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][6]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][6]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][6]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][6]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][6]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][6]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
            </tr>
          </table>

          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center" class="menu-table-2">
            <tr>
              <td valign="top" width="33.33%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][7]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][7]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][7]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][7]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][7]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][7]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>

              <td valign="top" width="33.33%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][8]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][8]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][8]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][8]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][8]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][8]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>

              <td valign="top" width="33.33%">
                <a href="@if($lang == "zh-hk"){{ $data['buttons'][9]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][9]->link_en }}@endif" style="text-decoration:none;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][9]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <!--[if gte mso 9]>
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][9]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        <![endif]-->
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        @if($lang == "zh-hk"){{ $data['buttons'][9]->title_zh }}@elseif($lang == "en"){{ $data['buttons'][9]->title_en }}@endif
                      </td>
                    </tr>
                  </table>
                </a>
              </td>
            </tr>
          </table>

          <!--[endif]-->

          <!--[if gte mso 9]>
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
            <tr>
              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][3]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][3]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][3]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][3]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                          </v:rect>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][3]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][3]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.subscribe')</a>
                      </td>
                    </tr>
                  </table>
              </td>
              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][4]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][4]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][4]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][4]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][4]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][4]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.unsubscribe')</a>
                      </td>
                    </tr>
                  </table>
              </td>
              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][5]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][5]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][5]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][5]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][5]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][5]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.cu_alumni_magazine')</a>
                      </td>
                    </tr>
                  </table>
              </td>
              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][6]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][6]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][6]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][6]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][6]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][6]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.cuhk_e_newsletter')</a>
                      </td>
                    </tr>
                  </table>
              </td>

              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][7]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][7]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][7]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][7]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][8]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][8]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.login_mycuhk')</a>
                      </td>
                    </tr>
                  </table>
              </td>

              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][8]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][8]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][8]->link_en }}@endif" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][8]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="@if($lang == "zh-hk"){{ $data['buttons'][8]->link_zh }}@elseif($lang == "en"){{ $data['buttons'][8]->link_en }}@endif" style="text-decoration:none; color: #818181;">@lang('master.aao_website')</a>
                      </td>
                    </tr>
                  </table>
              </td>

              <td valign="top" width="14.2857%">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" align="center">
                    <tr>
                      <td style="background-image:url('{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][9]->icon_zh }}'); background-repeat: no-repeat; background-size: contain !important; background-position: center !important;" width="25" height="25" align="center">
                        <a href="{{$data['protocol']}}://{{$data['domain']}}/{{$lang}}/search" style="text-decoration:none;">
                          <v:image xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block; width: 25px; height: 25px;" src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][9]->icon_zh }}" />
                          <v:rect xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false" style=" border: 0;display: inline-block;position: absolute; width: 25px; height:25px;">
                            <v:fill  opacity="0%" color="#000000”  />
                            <v:textbox inset="0,0,0,0">
                            </v:textbox>
                          </v:rect>
                        </a>
                      </td>
                    </tr>
                    <tr>
                      <td style="font-size: @if($lang == "zh-hk") 13px @else 12px @endif; color: #818181; text-align: center; padding-top: 6px; " align="center">
                        <a href="{{$data['protocol']}}://{{$data['domain']}}/{{$lang}}/search" style="text-decoration:none; color: #818181;">@lang('master.search')</a>
                      </td>
                    </tr>
                  </table>
              </td>
            </tr>
          </table>
          <![endif]-->

        </td>
      </tr>

      <!--[if gte mso 9]>
        <tr height="4">
          <td height="4">
            <img src="{{$data['protocol']}}://{{$data['domain']}}/images/web/seperator_page.jpg" width="710" height="4" alt="alt_text" border="0" style="width: 100%; max-width: 710px; height: 4;">
          </td>
        </tr>
      <![endif]-->
      <!--[if !mso]>-->

      <tr>
        <td style="line-height:4px; font-size:4px; background-color: #750F6D;" height="4">
        </td>
      </tr>
      <!--[endif]-->

      <tr>
        <td style="background-color: #ffffff; padding-top: 28px;">
          <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="218" align="center">
            <tr>
              <td width="30">
                <a href="{{ $data['buttons'][10]->link_zh }}" style="text-decoration:none;">
                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][10]->icon_zh }}" width="30" alt="alt_text" border="0" class="g-img">
                </a>
              </td>
              <td width="17">
              </td>
              <td width="30">
                <a href="{{ $data['buttons'][11]->link_zh }}" style="text-decoration:none;">
                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][11]->icon_zh }}" width="30" alt="alt_text" border="0" class="g-img">
                </a>
              </td>
              <td width="17">
              </td>
              <td width="30">
                <a href="{{ $data['buttons'][12]->link_zh }}" style="text-decoration:none;">
                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][12]->icon_zh }}" width="30" alt="alt_text" border="0" class="g-img">
                </a>
              </td>
              <td width="17">
              </td>
              <td width="30">
                <a href="{{ $data['buttons'][13]->link_zh }}" style="text-decoration:none;">
                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][13]->icon_zh }}" width="30" alt="alt_text" border="0" class="g-img">
                </a>
              </td>
              <td width="17">
              </td>
              <td width="30">
                <a href="{{ $data['buttons'][14]->link_zh }}" style="text-decoration:none;">
                  <img src="{{$data['protocol']}}://{{$data['domain']}}/images/icon/{{ $data['buttons'][14]->icon_zh }}" width="30" alt="alt_text" border="0" class="g-img">
                </a>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <tr>
        <td style="background-color: #ffffff; text-align: center; padding-top: 27px; padding-bottom: 32px; color: #750F6D; font-size: @if($lang == "zh-hk") 13px @else 12px @endif; ">
          @lang('master.copyright')<br />
          @lang('master.inquire')<br />
          @lang('master.address')<br />
          @lang('master.tel') @lang('master.fax')
        </td>
      </tr>

    </table>

  </center>
</body>
</html>
