@extends('layouts.admin')

@section('title', 'New Passage')

@section('content')

<h3>New Passage</h3>
<br />



<form action="/admin/action/insert/passage" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="issue">Issue</label>
    <select class="custom-select" id="issue" name="issue_id" required>
      <option value="">Select Issue</option>
      @foreach($issues as $issue)
         <option value="{{ $issue->id }}" @if (old('issue_id') == $issue->id || $issue_id == $issue->id) selected  @endif>#{{ $issue->number }} {{ $issue->date }}</option>
      @endforeach
    </select>
  </div>

  <div class="form-group">
    <label for="category">Category</label>
    <select class="custom-select" id="category" name="category_id" required>
      <option value="">Select Category</option>
      @foreach($categories as $category)
        @if($category->type != 3)
         <option value="{{ $category->id }}" @if (old('category_id') == $category->id || $category_id == $category->id) selected  @endif data-type="{{ $category->type }}">{{ $category->name_zh }} {{ $category->name_en }}</option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="form-group @if(old('category_type') == 4 || ($category_type != null && $category_type->id == 4)) d-block @endif" id="event">
    <label for="event_date">Event Date</label>
    <input type="text" class="form-control datepicker-date" id="event_date" name="event_date" placeholder="Select Date" autocomplete="off" value="{{ old('event_date') }}">
  </div>

  <div class="form-group">
    <label for="title_zh">Chinese Title</label>
    <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title" autocomplete="off" required value="{{ old('title_zh') }}">
  </div>

  <div class="form-group">
    <label for="title_en">English Title</label>
    <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title" autocomplete="off" required value="{{ old('title_en') }}">
  </div>

  <div class="form-group">
    <label for="link">Chinese Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link" autocomplete="off" value="{{ old('link_zh') }}">
    <small class="text-info">If this link is filled in, do not need to fill in the Chinese and English Content. (With http:// or https://)</small>
  </div>

  <div class="form-group">
    <label for="link">English Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link" autocomplete="off" value="{{ old('link_en') }}">
    <small class="text-info">If chinese link if filled in but this link is empty, this will follow the chinese link. (With http:// or https://)</small>
  </div>

  <div class="form-group files">
    <label>Upload Cover Image </label>
    <input type="file" class="form-control" name="cover">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center current-files d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn">Remove Image</button>
  </div>


  <div class="form-group">
    <label for="summernote">Chinese Content</label>
    <textarea id="summernote_zh" name="content_zh">{{ clean(old('content_zh')) }}</textarea>
  </div>

  <div class="form-group">
    <label for="summernote">English Content</label>
    <textarea id="summernote_en" name="content_en">{{ clean(old('content_en')) }}</textarea>
  </div>

  <div class="form-group">
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="publish" name="passage_publish" value="1">
      <label class="custom-control-label" for="publish">Publish</label>
    </div>
  </div>


  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
