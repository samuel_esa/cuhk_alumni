@extends('layouts.admin')

@section('title', 'New Category')

@section('content')

<h3>New Category</h3>
<br />

<form action="/admin/action/insert/category" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="category-name">Chinese Category Name</label>
    <input type="text" class="form-control" id="category-name" name="name_zh" placeholder="Category" autocomplete="off" required value="{{ old('name_zh') }}">
  </div>

  <div class="form-group">
    <label for="category-name">English Category Name</label>
    <input type="text" class="form-control" id="category-name" name="name_en" placeholder="Category" autocomplete="off" required value="{{ old('name_en') }}">
  </div>

  <div class="form-group files">
    <label>Upload Icon Image </label>
    <input type="file" class="form-control" name="icon">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center current-files d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn">Remove Image</button>
  </div>

  <div class="form-group">
    <label for="type">Display Type</label>
    <select class="custom-select" id="type" name="type" required>
      <option value="">Select Type</option>
      @foreach($category_types as $category_type)
         <option value="{{ $category_type->id }}" @if (old('type') == $category_type->id) selected  @endif>{{ $category_type->name }}</option>
      @endforeach
    </select>
  </div>

  <div class="form-group @if(old('type') == 1) d-block @endif" id="col">
    <label for="col_no">Column Number (1 - 4)</label>
    <input type="number" class="form-control" id="col_no" name="col_no" placeholder="Number of passage in one row" autocomplete="off" value="{{ old('col_no') }}">
  </div>

  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
