@extends('layouts.admin')

@section('title', 'Edit Issue')

@section('content')

<div class="flash-message"></div>
<div class="row justify-content-between mb-3">
  <div class="col-9">
    <a class="btn btn-primary" href="/zh-hk/preview/issue/{{ $issue->date }}" role="button">Preview - Chinese</a>
    <a class="btn btn-primary" href="/en/preview/issue/{{ $issue->date }}" role="button">Preview - English</a>
  </div>
  <div class="col-3 text-right">
    <form action="/admin/action/delete/issue" method="post" class="delete">
      <input type="hidden" name="id" value="{{ $issue->id }}" required>
      @csrf
      <button type="submit" class="btn btn-danger">Remove</button>
    </form>
  </div>
</div>

<h3>Edit Issue</h3>
<br />
<form action="/admin/action/update/issue" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col">
      <label for="issue-number">Edit Issue Number</label>
      <input type="text" class="form-control" id="issue-number" name="number" placeholder="Insert Number" autocomplete="off" value="{{ $issue->number }}" required>
    </div>

    <div class="form-group col">
      <label for="issue-date">Edit Issue Date</label>
      <input type="text" class="form-control datepicker" id="issue-date" name="date" placeholder="Select Date" autocomplete="off" value="{{ $issue->date }}" required>
    </div>
  </div>

  <div class="form-group">
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="publish" name="publish" @if($issue->publish == 1) checked @endif>
      <label class="custom-control-label" for="publish">Publish</label>
    </div>
  </div>

  <input type="hidden" name="id" value="{{ $issue->id }}" required>
  @csrf
  <button type="submit" class="btn btn-info">Update</button>
</form>

@foreach($categories as $category)

  <h4>{{$category->name_zh}} | {{$category->name_en}}</h4>

  @if($category->type == 3)

    <form action="/admin/action/update/issue_alumni" method="post" enctype="multipart/form-data">

      <div class="form-group">
        <label for="issue-link">Edit Alumni Link</label>
        <input type="text" class="form-control" id="issue-link" name="alumni_link" placeholder="Insert Link" value="{{ $issue->alumni_link }}" autocomplete="off">
        <small class="text-info">The Link format should start with http or https.</small>
      </div>

      <input type="hidden" name="id" value="{{ $issue->id }}" required>
      @csrf
      <button type="submit" class="btn btn-info">Update</button>
    </form>

  @else

    <div id="toolbar-{{$category->id}}">
      <a class="btn btn-success" href="/admin/passage/new/{{$id}}/{{$category->id}}" role="button"><i class="fas fa-plus"></i> Add New Passsage</a>
    </div>

    <table class="passage-table" data-toggle="table" data-url="/admin/action/select/passage/{{$id}}/{{$category->id}}" data-pagination="true" data-search="true" data-toolbar="#toolbar-{{$category->id}}" data-sort-stable="true">
      <thead>
        <tr>
          <th data-field="title_zh" data-sortable="true">Title</th>
          @if($category->type == 4)
            <th data-field="event_date" data-sortable="true">Event Date</th>
          @endif
          <th data-field="create_date" data-sortable="true">Create On</th>
          <th data-field="update_date" data-sortable="true">Last Update</th>
          <th data-field="passage_publish" data-sortable="true">Is Published</th>
          <th data-field="is_cover" data-formatter="setCoverFormatter" data-sortable="true">Set Cover</th>
          @if($category->type != 4)
            <th data-field="passage" data-formatter="sequenceFormatter" data-sortable="true">Sequence</th>
          @endif
          <th data-field="name" data-sortable="true">Author</th>
        </tr>
      </thead>
    </table>
    <small class="text-info">* Set Slider Sequence <strong>to or smaller than 0</strong> for not showing in the slider.</small>

  @endif

@endforeach



  <br />
  <br />
  <h3>Generate eDM for this Issue</h3>
  <div class="container">
    <div class="row">
      <div class="col text-center">
        Chinese Version<br />
        <a class="btn btn-outline-warning" href="/admin/action/generate/zh-hk/{{$id}}" role="button">Download</a>
      </div>
      <div class="col text-center">
        English Version<br />
        <a class="btn btn-outline-warning" href="/admin/action/generate/en/{{$id}}" role="button">Download</a>
      </div>
    </div>
  </div>

@endsection
