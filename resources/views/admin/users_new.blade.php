@extends('layouts.admin')

@section('title', 'New Issue')

@section('content')

<h3>New User</h3>
<br />

<form action="/admin/action/insert/users" method="post" enctype="multipart/form-data">
  <div class="form-row">
    <div class="form-group col">
      <label for="users-name">Name</label>
      <input type="text" class="form-control" id="users-name" name="name" placeholder="Insert Name" autocomplete="off" required value="{{ old('name') }}">
    </div>
    <div class="form-group col">
      <label for="users-email">Email</label>
      <input type="email" class="form-control" id="users-email" name="email" placeholder="Insert Email" autocomplete="off" required value="{{ old('email') }}">
    </div>
  </div>

  <div class="form-group">
    <label for="users-password">Password</label>
    <input type="password" class="form-control" id="users-password" name="password" placeholder="Insert Password" autocomplete="off" value="{{ old('password') }}">
  </div>
  <div class="form-group">
    <label for="users-confirm-password">Confirm Password</label>
    <input type="password" class="form-control" id="users-confirm-password" name="confirm_password" placeholder="Insert Confirm Password" autocomplete="off" value="{{ old('confirm_password') }}">
  </div>

  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
