@extends('layouts.admin')

@section('title', 'New News')

@section('content')

<h3>New Passage</h3>
<br />



<form action="/admin/action/insert/news" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="category">Category</label>
    <select class="custom-select" id="category" name="category_id" required>
      <option value="">Select Category</option>
      @foreach($categories as $category)
        @if($category->type != 3)
         <option value="{{ $category->id }}" @if (old('category_id') == $category->id) selected  @endif data-type="{{ $category->type }}">{{ $category->name_zh }} {{ $category->name_en }}</option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="form-group @if(old('category_type') == 4) d-block @endif" id="event">
    <label for="event_date">Event Date</label>
    <input type="text" class="form-control datepicker-date" id="event_date" name="event_date" placeholder="Select Date" autocomplete="off" value="{{ old('event_date') }}">
  </div>

  <div class="form-group">
    <label for="title_zh">Chinese Title</label>
    <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title" autocomplete="off" required value="{{ old('title_zh') }}">
  </div>

  <div class="form-group">
    <label for="title_en">English Title</label>
    <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title" autocomplete="off" required value="{{ old('title_en') }}">
  </div>

  <div class="form-group files">
    <label>Upload Cover Image </label>
    <input type="file" class="form-control upload-cover" name="cover">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group">
    <button type="button" class="btn btn-warning remove-img-btn d-none">Remove Image</button>
  </div>


  <div class="form-group">
    <label for="summernote">Chinese Content</label>
    <textarea id="summernote_zh" name="content_zh" required>{{ old('content_zh') }}</textarea>
  </div>

  <div class="form-group">
    <label for="summernote">English Content</label>
    <textarea id="summernote_en" name="content_en" required>{{ old('content_en') }}</textarea>
  </div>


  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
