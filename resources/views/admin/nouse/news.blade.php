@extends('layouts.admin')

@section('title', 'News')

@section('content')
<div id="toolbar">
  <a class="btn btn-success" href="/admin/news/new" role="button"><i class="fas fa-plus"></i> Add News</a>
</div>

<table id="news-table" data-toggle="table" data-url="/admin/action/select/news" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
    <thead>
      <tr>
        <th data-field="date" data-sortable="true">Issue</th>
        <th data-field="name_zh" data-sortable="true">Category</th>
        <th data-field="title_zh" data-sortable="true">Title</th>
        <th data-field="create_date" data-sortable="true">Create On</th>
        <th data-field="update_date" data-sortable="true">Last Update</th>
        <th data-field="approved" data-sortable="true">Is Approved</th>
      </tr>
    </thead>
  </table>
@endsection
