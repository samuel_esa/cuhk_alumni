@extends('layouts.admin')

@section('title', 'Edit News')

@section('content')

@if($news->approved == 0)
<div class="row">
  <div class="col-12 text-right">
    <form action="/admin/action/delete/news" method="post" class="delete">
      <input type="hidden" name="id" value="{{ $news->id }}" required>
      @csrf
      <button type="submit" class="btn btn-danger">Remove</button>
    </form>
  </div>
</div>
@endif

<h3>Edit News</h3>
<br />

<form action="/admin/action/update/news" method="post" enctype="multipart/form-data">

  <div class="form-group">
    <label for="category">Category</label>
    <select class="custom-select" id="category" name="category_id" required @if($news->approved == 1) disabled @endif>
      <option value="">Select Category</option>
      @foreach($categories as $category)
        @if($category->type != 3)
         <option value="{{ $category->id }}" @if($category->id == $news->category_id) selected @endif data-type="{{ $category->type }}">{{ $category->name_zh }} {{ $category->name_en }}</option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="form-group @if($news->type == 4) d-block @endif" id="event">
    <label for="event_date">Event Date</label>
    <input type="text" class="form-control datepicker-date" id="event_date" name="event_date" placeholder="Select Date" autocomplete="off" value="{{ $news->event_date }}">
  </div>

  <div class="form-group">
    <label for="title_zh">Chinese Title</label>
    <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title" autocomplete="off" value="{{ $news->title_zh }}" required @if($news->approved == 1) readonly @endif>
  </div>

  <div class="form-group">
    <label for="title_en">English Title</label>
    <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title" autocomplete="off" value="{{ $news->title_en }}" required @if($news->approved == 1) readonly @endif>
  </div>

  <div class="form-group files">
    <label>Upload Cover Image </label>
    <input type="file" class="form-control upload-cover" name="cover" @if($news->cover != "") style="background-image: url('/images/blog/{{ $news->cover }}'); outline: none" @endif @if($news->approved == 1) disabled @endif>
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  @if($news->approved == 0)
    <div class="form-group">
      <button type="button" class="btn btn-warning remove-img-btn @if($news->cover == "") d-none @endif">Remove Image</button>
    </div>
  @endif

  <div class="form-group">
    <label for="summernote_zh">Chinese Content</label>
    <textarea id="summernote_zh" name="content_zh" required>{{ $news->content_zh }}</textarea>
  </div>

  <div class="form-group">
    <label for="summernote_en">English Content</label>
    <textarea id="summernote_en" name="content_en" required>{{ $news->content_en }}</textarea>
  </div>

  @if($news->approved == 0)
    <input type="hidden" name="remove_img" value="0" required>

    <input type="hidden" name="id" value="{{ $news->id }}" required>
    @csrf
    <button type="submit" class="btn btn-info">Update</button>
  @else
    <input type="hidden" name="approved" value="{{ $news->approved }}" required>
  @endif


</form>
@endsection
