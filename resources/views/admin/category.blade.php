@extends('layouts.admin')

@section('title', 'Category')

@section('content')

<div class="flash-message"></div>

<div id="toolbar">
  <a class="btn btn-success" href="/admin/category/new" role="button"><i class="fas fa-plus"></i> Add New Category</a>
</div>

  <table id="category-table" data-toggle="table" data-url="/admin/action/select/category" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
    <thead>
      <tr>
        <th data-field="name_zh" data-sortable="true">Chinese Category</th>
        <th data-field="name_en" data-sortable="true">English Category</th>
        <th data-field="name" data-sortable="true">Display Type</th>
        <th data-field="category" data-formatter="sequenceFormatter" data-sortable="true">Sequence</th>
      </tr>
    </thead>
  </table>
@endsection
