@extends('layouts.admin')

@section('title', 'Buttons')

@section('content')

<h5>
  Index Page Bottom Buttons
</h5>
<hr />
<table id="buttons-table-1" data-toggle="table" data-url="/admin/action/select/button/1" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
  <thead>
    <tr>
      <th data-field="id" data-sortable="true">Id</th>
      <th data-field="icon_zh">Chinese Icon</th>
      <th data-field="icon_en">English Icon</th>
    </tr>
  </thead>
</table>

<h5 class="mt-5">
  Master Bottom Buttons
</h5>
<hr />
<table id="buttons-table-2" data-toggle="table" data-url="/admin/action/select/button/2" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
  <thead>
    <tr>
      <th data-field="id" data-sortable="true">Id</th>
      <th data-field="icon_zh">Icon</th>
      <th data-field="title_zh" data-sortable="true">Chinese Title</th>
      <th data-field="title_en" data-sortable="true">English Title</th>
    </tr>
  </thead>
</table>

<h5 class="mt-5">
  Social Media Buttons
</h5>
<hr />
<table id="buttons-table-3" data-toggle="table" data-url="/admin/action/select/button/3" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
  <thead>
    <tr>
      <th data-field="id" data-sortable="true">Id</th>
      <th data-field="icon_zh">Icon</th>
    </tr>
  </thead>
</table>

@endsection
