@extends('layouts.admin')

@section('title', 'Banner')

@section('content')
<div id="toolbar">
  <a class="btn btn-success" href="/admin/banner/new" role="button"><i class="fas fa-plus"></i> Add New Banner</a>
</div>

  <table id="banner-table" data-toggle="table" data-url="/admin/action/select/banner" data-pagination="true" data-search="true" data-toolbar="#toolbar" data-sort-stable="true">
    <thead>
      <tr>
        <th data-field="ads_title" data-sortable="true">Ads Name</th>
        <th data-field="start_date" data-sortable="true">Start Date</th>
        <th data-field="end_date" data-sortable="true">End Date</th>
        <th data-field="publish" data-sortable="true">Is Published</th>
      </tr>
    </thead>
  </table>
@endsection
