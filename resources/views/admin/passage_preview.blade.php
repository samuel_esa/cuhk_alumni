<?php use \App\Approve; ?>

@extends('layouts.master')

<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand">
            Preview Issue
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
              @if (Auth::user()->group >= 2)
                @if (strpos(Request::path(), 'admin/issues') !== false)
                  <li class="nav-item active">
                @else
                  <li class="nav-item">
                @endif
                      <a class="nav-link" href="/admin/issues/{{$issue->id}}">Back To Edit Issue</a>
                  </li>
              @endif
            </ul>

            @if($issue->publish == 0)
              <span class="badge badge-warning">Not Published</span>
            @else
              <span class="badge badge-success">Published</span>
            @endif

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>

@if($lang == "zh-hk")
  @section('title', $passage->title_zh)
@elseif($lang == "en")
  @section('title', $passage->title_en)
@endif

@section('content')

@empty($passage)
  <div class="container container-space text-center">
    @lang('master.no_issue')
  </div>
@endempty

@isset($passage)
  <section id="pagination" class="container">

    <div class="d-flex justify-content-between">

      <div id="issue">
          <div class="d-flex current-issue">
            <div class="issue-number">
              @if($lang == "zh-hk")
                第{{ $issue->number }}期
              @elseif($lang == "en")
                Issue No. {{ $issue->number }}
              @endif
            </div>
            <div class="issue-date">
                <span>
                  @if($lang == "zh-hk")
                    {{ date('Y年n月號', strtotime($issue->date)) }}
                  @elseif($lang == "en")
                    {{ date('M Y', strtotime($issue->date)) }}
                  @endif
                </span>
            </div>
          </div>
          <div class="issue-list d-none">
          </div>
      </div>

      <ul class="d-flex justify-content-end align-items-center">
        <li><a href="/{{ $lang }}">@lang('master.home')</a></li>
        <li class="seperator"></li>
        <li><a href="/zh-hk/preview{{$path}}">繁</a></li>
        <li><a href="/en/preview{{$path}}">ENG</a></li>  
      </ul>

    </div>

  </section>
@endisset

@isset($passage)

  @if($passage->cover != null && $passage->cover != "")

  <section class="passage-cover container" style="background-image: url('/images/blog/{{ $passage->cover}}');">
  </section>

  @endif

  <section class="passage-content container">

    @if($lang == "zh-hk")
      <h2>{{ $passage->title_zh }}</h2>
    @elseif($lang == "en")
      <h2>{{ $passage->title_en }}</h2>
    @endif

    <div class="passage-html">
      @if($lang == "zh-hk")
        {!! clean($passage->content_zh) !!}
      @elseif($lang == "en")
        {!! clean($passage->content_en) !!}
      @endif
    </div>
    <a href="/{{$lang}}/preview/issue/{{$issue->date}}" class="d-flex align-items-center back-btn"><div class="back-btn-icon background-image-contain"></div> @lang('master.back') </a>
  </section>

@endisset

@endsection
