@extends('layouts.admin')

@section('title', 'Passage')

@section('content')

<table id="approve-table" data-toggle="table" data-url="/admin/action/select/approve" data-pagination="true" data-search="true" data-sort-stable="true">
    <thead>
      <tr>
        <th data-field="id" data-sortable="true">number</th>
        <th data-field="name_zh" data-sortable="true">Category</th>
        <th data-field="title_zh" data-sortable="true">Title</th>
        <th data-field="create_date" data-sortable="true">Create On</th>
        <th data-field="update_date" data-sortable="true">Last Update</th>
        <th data-field="publisher" data-sortable="true">Author</th>
        <th data-field="publisher_organizer" data-sortable="true">Organization</th>
      </tr>
    </thead>
  </table>
@endsection
