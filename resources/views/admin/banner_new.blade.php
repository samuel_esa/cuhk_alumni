@extends('layouts.admin')

@section('title', 'New Banner')

@section('content')

<h3>New Banner</h3>
<br />

<form action="/admin/action/insert/banner" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="banner-title">Ads Name</label>
    <input type="text" class="form-control" name="ads_title" placeholder="Name" autocomplete="off" required value="{{ old('ads_title') }}">
  </div>

  <div class="form-group">
    <label for="link">Chinese Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link" autocomplete="off" value="{{ old('link_zh') }}">
    <small class="text-info">With http:// or https://</small>
  </div>

  <div class="form-group">
    <label for="link">English Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link" autocomplete="off" value="{{ old('link_en') }}">
    <small class="text-info">With http:// or https://</small>
  </div>

  <div class="form-group ads_pc_zh-files files">
    <label>Upload Chinese Ads Image - PC</label>
    <input type="file" class="form-control" name="ads_pc_zh">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_pc_zh d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_pc_zh">Remove Image</button>
  </div>

  <div class="form-group ads_mobile_zh-files files">
    <label>Upload Chinese Ads Image - Mobile</label>
    <input type="file" class="form-control" name="ads_mobile_zh">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_mobile_zh d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_mobile_zh">Remove Image</button>
  </div>

  <div class="form-group ads_pc_en-files files">
    <label>Upload English Ads Image - PC</label>
    <input type="file" class="form-control" name="ads_pc_en">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_pc_en d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_pc_en">Remove Image</button>
  </div>

  <div class="form-group ads_mobile_en-files files">
    <label>Upload English Ads Image - Mobile</label>
    <input type="file" class="form-control" name="ads_mobile_en">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center ads_mobile_en d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn" data-banner="ads_mobile_en">Remove Image</button>
  </div>

  <div class="form-group">
    <label>Ads Date Range</label>
    <div class="input-group daterange">
      <input type="text" class="form-control" name="start_date" placeholder="Start Date" readonly required value="{{ old('start_date') }}">
      <div class="input-group-prepend input-group-append">
        <span class="input-group-text">to</span>
      </div>
      <input type="text" class="form-control" name="end_date" placeholder="End Date" readonly required value="{{ old('end_date') }}">
    </div>
  </div>

  <div class="form-group">
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="publish" name="publish" value="1">
      <label class="custom-control-label" for="publish">Publish</label>
    </div>
  </div>

  @csrf
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
