@extends('layouts.admin')

@section('title', 'Edit Passage')

@section('content')

<div class="row">
  <div class="col-12 text-right">
    <form action="/admin/action/delete/approve" method="post" class="delete">
      <input type="hidden" name="id" value="{{ $passage->id }}" required>
      @csrf
      <button type="submit" class="btn btn-danger">Remove</button>
    </form>
  </div>
</div>

<h3>Edit Passage</h3>
<br />

<form action="/admin/action/update/approve" method="post" enctype="multipart/form-data">

  <div class="card mb-3">
    <div class="card-body">

      @if($passage->publisher != '')
        <div class="form-group">
          <label for="author">Author</label>
          <h6>{{ $passage->publisher }}</h6>
        </div>
      @endif

      @if($passage->publisher_organizer != '')
        <div class="form-group">
          <label for="organization">Organization</label>
          <h6>{{ $passage->publisher_organizer }}</h6>
        </div>
      @endif

      @if($passage->publisher_email != '')
        <div class="form-group">
          <label for="email">Email</label>
          <a href="mailto: {{ $passage->publisher_email }}"><h6>{{ $passage->publisher_email }}</h6></a>
        </div>
      @endif

      @if($passage->publisher_phone != '')
        <div class="form-group">
          <label for="phone">Contact Number</label>
          <a href="tel: {{ $passage->publisher_phone }}"><h6>{{ $passage->publisher_phone }}</h6></a>
        </div>
      @endif

      @if($passage->approve_id != '')
        <div class="form-group">
          <label for="link-zh">Preview Link - Chinese</label>
          <a href="/zh-hk/preview/news/{{ $passage->approve_id }}"><h6>{{ url('/') . "/zh-hk/preview/news/{$passage->approve_id}" }}</h6></a>
        </div>

        <div class="form-group">
          <label for="link-en">Preview Link - English</label>
          <a href="/en/preview/news/{{ $passage->approve_id }}"><h6>{{ url('/') . "/en/preview/news/{$passage->approve_id}" }}</h6></a>
        </div>
      @endif
    </div>
  </div>

  <div class="form-group">
    <label for="issue">Issue</label>
    <select class="custom-select" id="issue" name="issue_id" required style="background-color: #ffa;">
      <option value="">Select Issue</option>
      @foreach($issues as $issue)
         <option value="{{ $issue->id }}"> #{{ $issue->number }} {{ $issue->date }}</option>
      @endforeach
    </select>
  </div>

  <div class="form-group">
    <label for="category">Category</label>
    <select class="custom-select" id="category" name="category_id" required>
      <option value="">Select Category</option>
      @foreach($categories as $category)
        @if($category->type != 3)
         <option value="{{ $category->id }}" @if($category->id == $passage->category_id) selected @endif data-type="{{ $category->type }}">{{ $category->name_zh }} {{ $category->name_en }}</option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="form-group @if($passage->type == 4) d-block @endif" id="event">
    <label for="event_date">Event Date</label>
    <input type="text" class="form-control datepicker-date" id="event_date" name="event_date" placeholder="Select Date" autocomplete="off" value="{{ $passage->event_date }}">
  </div>

  <div class="form-group">
    <label for="title_zh">Chinese Title</label>
    <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title" autocomplete="off" value="{{ $passage->title_zh }}" required>
  </div>

  <div class="form-group">
    <label for="title_en">English Title</label>
    <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title" autocomplete="off" value="{{ $passage->title_en }}" required>
  </div>

  <div class="form-group">
    <label for="link">Chinese Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link" autocomplete="off" value="{{ $passage->link_zh }}">
    <small class="text-info">If link is filled in, do not need to fill in the Chinese and English Content. (With http:// or https://)</small>
  </div>

  <div class="form-group">
    <label for="link">English Link (For External Passage)</label>
    <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link" autocomplete="off" value="{{ $passage->link_en }}">
    <small class="text-info">If chinese link if filled in but this link is empty, this will follow the chinese link. (With http:// or https://)</small>
  </div>

  <div class="form-group files @if($passage->cover != "") d-none @endif">
    <label>Upload Cover Image </label>
    <input type="file" class="form-control" name="cover">
    <small class="text-danger">File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb.</small>
  </div>

  <div class="form-group text-center current-files @if($passage->cover == "") d-none @endif">
    <div class="upload-cover background-image-contain" @if($passage->cover != "") style="background-image: url('/images/blog/{{ $passage->cover }}');" @endif>
    </div>
    <button type="button" class="btn btn-warning remove-img-btn">Remove Image</button>
    <a href="/images/blog/{{ $passage->cover }}" class="btn btn-success download-img-btn" download>Download Image</a>
  </div>

  <div class="form-group">
    <label for="summernote_zh">Chinese Content</label>
    <textarea id="summernote_zh" name="content_zh">{{ clean($passage->content_zh) }}</textarea>
  </div>

  <div class="form-group">
    <label for="summernote_en">English Content</label>
    <textarea id="summernote_en" name="content_en">{{ clean($passage->content_en) }}</textarea>
  </div>

  <div class="form-group">
    <div class="custom-control custom-switch">
      <input type="checkbox" class="custom-control-input" id="publish" name="passage_publish" @if($passage->passage_publish == 1) checked @endif>
      <label class="custom-control-label" for="publish">Publish</label>
    </div>
  </div>

  <input type="hidden" name="remove_img" value="0" required>

  <input type="hidden" name="id" value="{{ $passage->id }}" required>
  @csrf
  <button type="submit" class="btn btn-info">Approve</button>
</form>
@endsection
