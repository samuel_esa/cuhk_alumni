@extends('layouts.master')

@section('title', __('master.search'))

@section('content')

  <section id="pagination" class="container">

    <div class="d-flex justify-content-end">

      <ul class="d-flex justify-content-end align-items-center">
        <li><a href="/{{ $lang }}">@lang('master.home')</a></li>
        <li class="seperator"></li>
        <kanhanbypass>
          <li><a href="http://enews.alumni.cuhk.edu.hk/zh-hk/search">繁</a></li>
          <li><a href="http://translate.itsc.cuhk.edu.hk/uniTS/{{$host}}/zh-hk/search">简</a></li>
          <li><a href="http://enews.alumni.cuhk.edu.hk/en/search">ENG</a></li>
        </kanhanbypass>
      </ul>

    </div>

  </section>

  <section id="search" class="container">

    <div class="row justify-content-center">
      <div class="input-group">
        <input type="text" id="keywords" class="form-control" placeholder="@lang('master.search')">
        <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button"><div class="search-icon background-image-contain"></div></button>
        </div>
      </div>
    </div>

    <div class="text-center" id="serach-number"></div>

    <div id="search-result">
    </div>

  </section>

@endsection
