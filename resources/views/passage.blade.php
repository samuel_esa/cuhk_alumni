@extends('layouts.master')

@if($lang == "zh-hk")
  @section('title', $passage->title_zh)
@elseif($lang == "en")
  @section('title', $passage->title_en)
@endif

@section('content')

@empty($passage)
  <div class="container container-space text-center">
    @lang('master.no_issue')
  </div>
@endempty

@isset($passage)
  <section id="pagination" class="container">

    <div class="d-flex justify-content-between">

      <div id="issue">
          <div class="d-flex current-issue">
            <div class="issue-number">
              @if($lang == "zh-hk")
                第{{ $issue->number }}期
              @elseif($lang == "en")
                Issue No. {{ $issue->number }}
              @endif
            </div>
            <div  class="issue-date">
                <span>
                  @if($lang == "zh-hk")
                    {{ date('Y年n月號', strtotime($issue->date)) }}
                  @elseif($lang == "en")
                    {{ date('M Y', strtotime($issue->date)) }}
                  @endif
                </span>
                <i class="fas fa-chevron-down"></i>
            </div>
          </div>
          <div class="issue-list d-none">
            @foreach($issues as $is)
              <div class="seperator"></div>
              <a href="/{{$lang}}/issue/{{ $is->date }}" class="d-flex issue-row">
                <div class="issue-number">
                  @if($lang == "zh-hk")
                    第{{ $is->number }}期
                  @elseif($lang == "en")
                    Issue No. {{ $is->number }}
                  @endif
                </div>
                <div class="issue-date">
                    <span>
                      @if($lang == "zh-hk")
                        {{ date('Y年n月號', strtotime($is->date)) }}
                      @elseif($lang == "en")
                        {{ date('M Y', strtotime($is->date)) }}
                      @endif
                    </span>
                </div>
              </a>
            @endforeach
            <div class="seperator"></div>
            <a href="http://cloud.itsc.cuhk.edu.hk/enewsasp/app/article.aspx/40BE193DA0F8441B6E570B9B84D4A8F5/" target="_blank" class="d-flex issue-row">
              <div class="issue-number">
                @if($lang == "zh-hk")
                  過去期數
                @elseif($lang == "en")
                  Past issues
                @endif
              </div>
            </a>
          </div>
      </div>

      <ul class="d-flex justify-content-end align-items-center">
        <li><a href="/{{ $lang }}">@lang('master.home')</a></li>
        <li class="seperator"></li>
        <kanhanbypass>
          <li><a href="http://enews.alumni.cuhk.edu.hk/zh-hk{{$path}}">繁</a></li>
          <li><a href="http://translate.itsc.cuhk.edu.hk/uniTS/{{$host}}/zh-hk{{$path}}">简</a></li>
          <li><a href="http://enews.alumni.cuhk.edu.hk/en{{$path}}">ENG</a></li>
        </kanhanbypass>
      </ul>

    </div>

  </section>
@endisset

@isset($passage)

  @if($passage->cover != null && $passage->cover != "")

  <section class="passage-cover container" style="background-image: url('/images/blog/{{ $passage->cover}}');">
  </section>

  @endif

  <section class="passage-content container">

    @if($lang == "zh-hk")
      <h2>{{ $passage->title_zh }}</h2>
    @elseif($lang == "en")
      <h2>{{ $passage->title_en }}</h2>
    @endif

    <div class="passage-html">
      @if($lang == "zh-hk")
        {!! clean($passage->content_zh) !!}
      @elseif($lang == "en")
        {!! clean($passage->content_en) !!}
      @endif
    </div>
    <a href="/{{$lang}}/issue/{{$issue->date}}" class="d-flex align-items-center back-btn"><div class="back-btn-icon background-image-contain"></div> @lang('master.back') </a>
  </section>

@endisset

@endsection
