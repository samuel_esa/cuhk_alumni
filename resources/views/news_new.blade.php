@extends('layouts.master')

@section('title', 'Web Form')

@section('content')

<form action="/action/insert/news" class="container container-space" method="post" enctype="multipart/form-data">

  <div class="container">
    @if (session('alert-success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
          {{ session('alert-success') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
    @endif

    @if (session('alert-error'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          {{ session('alert-error') }}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>
    @endif
  </div>

  <h3>News Submission | 提交資料</h3>
  <br />

  <div class="form-group">
    <label for="category">Category ｜ 類別 <span class="text-danger">*</span></label>
    <select class="custom-select" id="category" name="category_id" required  oninvalid="this.setCustomValidity('Must fill in｜必須選擇')"
    oninput="this.setCustomValidity('')">
      <option value="">Select Category ｜ 選擇類別</option>
      @foreach($categories as $category)
        @if($category->type != 3)
         <option value="{{ $category->id }}" @if (old('category_id') == $category->id) selected  @endif data-type="{{ $category->type }}">{{ $category->name_zh }} {{ $category->name_en }}</option>
        @endif
      @endforeach
    </select>
  </div>

  <div class="form-group @if(old('category_type') == 4) d-block @endif" id="event">
    <label for="event_date">Event Date ｜ 活動日期</label>
    <input type="text" class="form-control datepicker-date" id="event_date" name="event_date" placeholder="Select Date ｜ 選擇日期" autocomplete="off" value="{{ old('event_date') }}">
  </div>

  <div class="form-group">
    <label for="title_zh">Chinese Title ｜ 中文標題 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="title_zh" name="title_zh" placeholder="Chinese Title ｜ 中文標題" autocomplete="off" required value="{{ old('title_zh') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    <label for="title_en">English Title ｜ 英文標題 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="title_en" name="title_en" placeholder="English Title ｜ 英文標題" autocomplete="off" required value="{{ old('title_en') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    <label for="link">Insert Chinese link for external content ｜ 輸入外部中文網站連結</label>
    <input type="text" class="form-control" id="link_zh" name="link_zh" placeholder="Chinese External Link ｜ 外部中文網站連結" autocomplete="off" value="{{ old('link_zh') }}">
    <small class="text-info">
      If this column is completed, do not fill in "Chinese and English Content". (With http:// or https://)<br />
      如果輸入外部網站連結，則不用填寫中文及英文內容。（網址前面必須加上http:// 或 https://）
    </small>
  </div>

  <div class="form-group">
    <label for="link">Insert English link for external content ｜ 輸入外部英文網站連結</label>
    <input type="text" class="form-control" id="link_en" name="link_en" placeholder="English External Link ｜ 外部英文網站連結" autocomplete="off" value="{{ old('link_en') }}">
    <small class="text-info">
      If this column is completed, do not fill in "Chinese and English Content". (With http:// or https://)<br />
      如果輸入外部網站連結，則不用填寫中文及英文內容。（網址前面必須加上http:// 或 https://）
    </small>
  </div>

  <div class="form-group files">
    <label>Upload Cover Image ｜ 上載封面圖片 <span class="text-danger">*</span></label>
    <input type="file" class="form-control" name="cover">
    <small class="text-danger">
      File format support: jpeg,png,jpg,gif,svg. Max. file size: 2048kb. Recommended Resolution: 960 x 520 or above<br />
      支援圖片格式：jpeg,png,jpg,gif,svg。圖片大小上限：2048kb。建議圖片解析度：960 x 520 或以上
    </small>
  </div>

  <div class="form-group text-center current-files d-none">
    <div class="upload-cover background-image-contain">
    </div>
    <button type="button" class="btn btn-warning remove-img-btn">Remove Image | 移除圖片</button>
  </div>


  <div class="form-group">
    <label for="summernote">Chinese Content ｜ 中文內容</label>
    <textarea id="summernote_zh" name="content_zh">{{ clean(old('content_zh')) }}</textarea>
  </div>

  <div class="form-group">
    <label for="summernote">English Content ｜ 英文內容</label>
    <textarea id="summernote_en" name="content_en">{{ clean(old('content_en')) }}</textarea>
  </div>

  <div class="form-group">
    <span class="text-danger">*</span> The information provided below will be for internal use only.<br />
    以下資料只供內部使用，將不會刊登。
  </div>

  <div class="form-group">
    <label for="publisher">Contact Person ｜ 聯絡人 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="publisher" name="publisher" placeholder="Contact Person ｜ 聯絡人" autocomplete="off" required value="{{ old('publisher') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    <label for="publisher_organizer">Organization ｜ 組織 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="publisher_organizer" name="publisher_organizer" placeholder="Organization ｜ 組織" autocomplete="off" required value="{{ old('publisher_organizer') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    <label for="publisher">Email ｜ 電郵 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="publisher_email" name="publisher_email" placeholder="Email ｜ 電郵" autocomplete="off" required value="{{ old('publisher_email') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    <label for="publisher">Contact Number ｜ 聯絡電話 <span class="text-danger">*</span></label>
    <input type="text" class="form-control" id="publisher_phone" name="publisher_phone" placeholder="Contact Number ｜ 聯絡電話" autocomplete="off" required value="{{ old('publisher_phone') }}" oninvalid="this.setCustomValidity('Must fill in｜必須填寫')"
    oninput="this.setCustomValidity('')">
  </div>

  <div class="form-group">
    Disclaimer<br />
    The personal data collected will be used by Alumni Affairs Office (AAO) and the authorised personnel for processing captioned purposes only. All personal data you provided will not be disclosed to any third parties unless with your prior consent.<br />
    免責條款<br />
    收集的個人資料將由校友事務處（AAO）和授權人員使用，僅用於中大校友電子月報。除非您事先同意，否則您提供的所有個人資料將不會被披露給任何第三方。
  </div>


  @csrf
  <button type="submit" class="btn btn-primary">Submit | 提交</button>
</form>

<script src="https://www.google.com/recaptcha/api.js?render=6LdXH90UAAAAAC9sJ7SJYkmqKknljrzn4EbfxfdQ"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('6LdXH90UAAAAAC9sJ7SJYkmqKknljrzn4EbfxfdQ', {action: 'homepage'}).then(function(token) {
      $.ajax({
         type: 'POST',
         url: '/news/recaptcha',
         data: {
           token: token
         },
         headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
         success: function (res) {
             if(!res){
               $("form").remove();
             }
         }
     });
    });
});
</script>

@endsection
